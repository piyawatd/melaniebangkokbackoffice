package melaniebangkokbackoffice

class News {

    String titleTh
    String titleEn
    String titleCh
    String descriptionTh
    String descriptionEn
    String descriptionCh
    String detailTh
    String detailEn
    String detailCh
    Boolean active
    String imageTh
    String imageEn
    String imageCh
    Date dateCreated
    Date lastUpdated

    static constraints = {
        titleTh blank: false
        titleEn blank: true,nullable: true
        titleCh blank: true,nullable: true
        descriptionTh blank: false
        descriptionEn blank: true,nullable: true
        descriptionCh blank: true,nullable: true
        detailTh blank: false
        detailEn blank: true,nullable: true
        detailCh blank: true,nullable: true
        active nullable: true
        imageTh blank: true,nullable: true
        imageEn blank: true,nullable: true
        imageCh blank: true,nullable: true
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'sequence_news_id']
        descriptionTh type: "text"
        descriptionEn type: "text"
        descriptionCh type: "text"
        detailTh type: "text"
        detailEn type: "text"
        detailCh type: "text"
    }
}
