package melaniebangkokbackoffice

class GalleryImage {

    String imageName
    Integer sequence
    Date dateCreated
    Date lastUpdated

    static belongsTo = [gallery:Gallery]

    static constraints = {
        imageName blank: false
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'sequence_gallery_image_id']
    }
}
