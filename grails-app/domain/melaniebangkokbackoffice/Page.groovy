package melaniebangkokbackoffice

class Page {

    String titleTh
    String titleEn
    String titleCh
    String detailTh
    String detailEn
    String detailCh
    String alias
    Date dateCreated
    Date lastUpdated

    static constraints = {
        titleTh blank: false
        titleEn blank: true,nullable: true
        titleCh blank: true,nullable: true
        detailTh blank: false
        detailEn blank: true,nullable: true
        detailCh blank: true,nullable: true
        alias blank: true,nullable: true
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'sequence_page_id']
        detailTh type: "text"
        detailEn type: "text"
        detailCh type: "text"
    }
}
