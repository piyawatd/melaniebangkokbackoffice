package melaniebangkokbackoffice

class Banner {

    String titleTh
    String titleEn
    String titleCh
    String captionTh
    String captionEn
    String captionCh
    Integer sequence
    Boolean active
    String imageTh
    String imageEn
    String imageCh
    Date dateCreated
    Date lastUpdated

    static constraints = {
        titleTh blank: false
        titleEn blank: true,nullable: true
        titleCh blank: true,nullable: true
        captionTh blank: false
        captionEn blank: true,nullable: true
        captionCh blank: true,nullable: true
        sequence nullable: false
        active nullable: true
        imageTh blank: false
        imageEn blank: true,nullable: true
        imageCh blank: true,nullable: true
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'sequence_banner_id']
        captionTh type: "text"
        captionEn type: "text"
        captionCh type: "text"
        sequence defaultValue:"1"
        active defaultValue:"true"
    }
}
