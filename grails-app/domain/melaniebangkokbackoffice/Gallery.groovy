package melaniebangkokbackoffice

class Gallery {

    String titleTh
    String titleEn
    String titleCh
    String detailTh
    String detailEn
    String detailCh
    String imageName
    Date dateCreated
    Date lastUpdated

    static hasMany = [galleryImages:GalleryImage]

    static constraints = {
        titleTh blank: false
        titleEn blank: true,nullable: true
        titleCh blank: true,nullable: true
        detailTh blank: false
        detailEn blank: true,nullable: true
        detailCh blank: true,nullable: true
        imageName blank: false
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'sequence_gallery_id']
        detailTh type: "text"
        detailEn type: "text"
        detailCh type: "text"
    }
}
