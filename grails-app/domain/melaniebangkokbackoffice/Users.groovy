package melaniebangkokbackoffice

class Users {

    String username
    String password
    String firstName
    String lastName
    String email
    Integer userLevel
    Date dateCreated
    Date lastUpdated

    static constraints = {
        username blank: false
        password blank: false
        firstName blank: false
        lastName blank: false
        email blank: false,email: true
        userLevel nullable: false
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'sequence_user_id']
        userLevel defaultValue:"0",comment: "0 = user,1 = admin,2 = saadmin"
    }

    def beforeInsert()
    {
        this.password = this.password.encodeAsSHA1()
    }

    def beforeUpdate()
    {
        this.password = this.password.encodeAsSHA1()
    }
}
