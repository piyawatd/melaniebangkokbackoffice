<?php
include('libs/main-config.php');
include('libs/database.php');
$db = new database();

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
	<meta name="twitter:widgets:theme" content="light">
	<meta property="og:title" content="Melanie Bangkok ถนนจันทร์-สาทร" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="http://www.melaniebangkok.com/images/slide3.jpg" />
	<meta property="og:description" content="Melanie Bangkok ถนนจันทร์-สาทร" />
    <meta name="description" content="Melanie Bangkok ถนนจันทร์-สาทร"/>
	<title>Melanie Bangkok ถนนจันทร์-สาทร</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/theme-mountain-favicon.ico">

	<!-- Font -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700%7CLato:300,400,700' rel='stylesheet' type='text/css'>
	
	<!-- Css -->
	<link rel="stylesheet" href="css/core.min.css" />
	<link rel="stylesheet" href="css/skin-architecture.css" />
    <link rel="stylesheet" href="css/them.css" />
	<!--[if lt IE 9]>
    	<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<style>
.text-box{
	margin-bottom:2px;	
	padding-left:35px;
	padding-right:35px;
	color:#fff;
	font-size:18px;
	width:100%;
    text-align: left;
}
.text-box input[type="text"]{
	width:100%;
	border:2px #ac9766 solid;
	height:30px;
	background:none;
	color:#ac9766;
	padding:5px;
    margin: 0;
	
}
 input[type="email"]{
	width:100%;
	border:2px #ac9766 solid;
	height:30px;
	background:none;
	color:#ac9766;
	padding:5px;
    margin: 0;
	
}
 label {
    padding-left:2px;
    width:130px;
    text-transform: uppercase;
    display:inline-block;
    font-weight: normal;
    margin: 0;
    text-align: left;
    
}
input[type=checkbox].css-checkbox {
							position:absolute; z-index:-1000; left:-1000px; overflow: hidden; clip: rect(0 0 0 0); height:1px; width:1px; margin:-1px; padding:0; border:0;
						}

				input[type=checkbox].css-checkbox + label.css-label {
							padding-left:22px;
							height:17px; 
							display:inline-block;
							line-height:17px;
							background-repeat:no-repeat;
							background-position: 0 0;
							font-size:17px;
							vertical-align:middle;
							cursor:pointer;
                            color: #fff;

						}

						input[type=checkbox].css-checkbox:checked + label.css-label {
							background-position: 0 -17px;
						}
						label.css-label {
				background-image:url(images/bgck.png);
				-webkit-touch-callout: none;
				-webkit-user-select: none;
				-khtml-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;
			}
      .bkg-grey-light {
    background: #AC9766 url(images/bg_nav.png) ;
    background-size: cover;
    border-color: #AC9766;
}      
.bkg-grey-light {
    background: #AC9766 url(images/bg_nav.png) ;
    background-size: cover;
    border-color: #AC9766;
}      
</style>
</head>
<body class="shop home-page" id="top">

	<!-- Side Navigation Menu -->
	<aside class="side-navigation-wrapper enter-right" data-no-scrollbar data-animation="scale-in">
		<div class="side-navigation-scroll-pane">
			<div class="side-navigation-inner">
				<div class="side-navigation-header">
					<div class="navigation-hide side-nav-hide">
						<a href="#">
							<span class="icon-cancel medium"></span>
						</a>
					</div>
				</div>
				<nav class="side-navigation">
					<ul>
					 <li><a href="index.php">HOME</a></li>
                                    <li><a href="project.php" >PROJECT</a></li>
                                    <li><a href="unit.php">UNIT PLAN</a></li>
                                    <li><a href="#gallery" class="scroll-link">GALLERY</a></li>
                                    <li><a href="#news" class="scroll-link">NEWS & PROGRESS</a></li>
                                    <li><a href="contact.php" >CONTACT</a></li>
                                    <li><a href="developer.php">DEVELOPER</a></li>
					</ul>
				</nav>
				<div class="side-navigation-footer">
					<ul class="social-list list-horizontal">
						<li><a href="#"><span class="icon-twitter small"></span></a></li>
						<li><a href="#"><span class="icon-facebook small"></span></a></li>
						<li><a href="#"><span class="icon-instagram small"></span></a></li>
					</ul>
					<p class="copyright no-margin-bottom">&copy; 2016 Melanie Bangkok.</p>
				</div>
			</div>
		</div>
	</aside>
	<!-- Side Navigation Menu End -->

	<div class="wrapper reveal-side-navigation">
		<div class="wrapper-inner">

			<!-- Header -->
			<header class="header header-fixed header-bottom header-fixed-on-mobile header-transparent" data-sticky-threshold="window-height" data-bkg-threshold="100">
				<div class="header-inner">
					<div class="row nav-bar">
						<div class="column width-12 nav-bar-inner">
                        <div class="pull-right"><a href="<?=curPageURL()?>" style="color: #AC9766;"><strong>TH</strong></a> | <a style="color: #AC9766;" href="<?=curPageURLEn()?>"><strong>EN</strong></a></div>
							<div class="logo">
								<div class="logo-inner">
									<a href="index.php"><img src="images/logo.png" alt="melanine Logo" /></a>
									<a href="index.php"><img src="images/logo.png" alt="melanine Logo" /></a>
								</div>
							</div>
							<nav class="navigation nav-block secondary-navigation nav-right">
								<ul>
									<!-- Dropdown Cart Overview
									<li>
										<div class="dropdown">
											<a href="#" class="nav-icon cart button no-page-fade"><span class="cart-indication"><span class="icon-shopping-cart"></span> <span class="badge">3</span></span></a>
											<ul class="dropdown-list custom-content cart-overview">
												<li class="cart-item">
													<a href="single-product.html" class="product-thumbnail">
														<img src="images/design-agency/portfolio/grid/no-margins/project-6-square.jpg" alt="" />
													</a>
													<div class="product-details">
														<a href="single-product.html" class="product-title">
															Cotton Jump Suit
														</a>
														<span class="product-quantity">2 x</span>
														<span class="product-price"><span class="currency">$</span>15.00</span>
														<a href="#" class="product-remove icon-cancel"></a>
													</div>
												</li>
												<li class="cart-item">
													<a href="single-product.html" class="product-thumbnail">
														<img src="images/design-agency/portfolio/grid/no-margins/project-7-square.jpg" alt="" />
													</a>
													<div class="product-details">
														<a href="single-product.html" class="product-title">
															Cotton Jump Suit
														</a>
														<span class="product-quantity">2 x</span>
														<span class="product-price"><span class="currency">$</span>15.00</span>
														<a href="#" class="product-remove icon-cancel"></a>
													</div>
												</li>
												<li class="cart-item">
													<a href="single-product.html" class="product-thumbnail">
														<img src="images/design-agency/portfolio/grid/no-margins/project-8-square.jpg" alt="" />
													</a>
													<div class="product-details">
														<a href="single-product.html" class="product-title">
															Cotton Jump Suit
														</a>
														<span class="product-quantity">2 x</span>
														<span class="product-price"><span class="currency">$</span>15.00</span>
														<a href="#" class="product-remove icon-cancel"></a>
													</div>
												</li>
												<li class="cart-subtotal">
													Sub Total
													<span class="amount"><span class="currency">$</span>15.00</span>
												</li>
												<li class="cart-actions">
													<a href="cart.html" class="view-cart">View Cart</a>
													<a href="checkout.html" class="checkout button small"><span class="icon-check"></span> Checkout</a>
												</li>
											</ul>
										</div>
									</li> -->
									<!-- Dropdown Search Module
									<li>
										<div class="dropdown">
											<a href="#" class="nav-icon search button no-page-fade"><span class="icon-magnifying-glass"></span></a>
											<div class="dropdown-list custom-content cart-overview">
												<div class="search-form-container site-search">
													<form action="#" method="get" novalidate>
														<div class="row">
															<div class="column width-12">
																<div class="field-wrapper">
																	<input type="text" name="search" class="form-search form-element no-margin-bottom" placeholder="type &amp; hit enter...">
																	<span class="border"></span>
																</div>
															</div>
														</div>
													</form>
													<div class="form-response"></div>
												</div>
											</div>
										</div>
									</li> -->
									<li class="aux-navigation hide">
										<!-- Aux Navigation -->
										<a href="#" class="navigation-show side-nav-show nav-icon">
											<span class="icon-menu"></span>
										</a>
									</li>
								</ul>
							</nav>
							<nav class="navigation nav-block primary-navigation nav-right no-margin-right">
								<ul>
                                    <li><a href="index.php">HOME</a></li>
                                    <li><a href="project.php" >PROJECT</a></li>
                                    <li><a href="unit.php">UNIT PLAN</a></li>
                                    <li><a href="#gallery" class="scroll-link">GALLERY</a></li>
                                    <li><a href="#news" class="scroll-link">NEWS & PROGRESS</a></li>
                                    <li><a href="contact.php" >CONTACT</a></li>
                                    <li><a href="developer.php">DEVELOPER</a></li>
                                    
                                </ul>
							</nav>
						</div>
					</div>
				</div>
			</header>
			<!-- Header End -->

			<!-- Content -->
			<div class="content clearfix">

				<!-- Fullscreen Slider Section -->
				<section class="section-block featured-media window-height tm-slider-parallax-container">
					<div id="homeslide" class="tm-slider-container full-width-slider pagination-top" data-featured-slider data-parallax data-parallax-fade-out data-animation="fade" data-auto-advance data-auto-advance-interval="4000" data-nav-pagination="false" data-scale-under="960" data-progress-bar="false">
						<ul class="tms-slides">
                        <? $option_py = "select * FROM tb_banner where status=1 order by Sequence DESC";
$i=1;
$query_pd2 = $db->query($option_py);
while($rs_pd = $db->get($query_pd2)){
?>
							<li class="tms-slide" data-image data-force-fit>
                            	<div class="tms-content bgcab  show1 hide-on-mobile" style="height:200px !important" >
								
									<div style="padding: 20px; <? if($_GET['thank']==1){?> line-height: 30px; <? }?>">
                                    <? if($_GET['thank']==1){?>
                                     <div>ขอบคุณสำหรับข้อมูล<br />ทางทีมงานจะติดต่อกลับท่านโดยเร็วที่สุด</div>
                                    <? }?>
											     <h1 style="color: #fff;" class="ft-db-adama">โลกส่วนตัว</h1>
                                                 <h2 style="color: #fff;" class="ft-db-adama">ที่เชื่อมทุกมุมเมืองให้อยู่ใกล้ๆคุณ</h2>
                                                 <div>ถนนจันทร์-สาทร<br />ราคาเริ่มต้น 3.8 ล้านบาท</div><br />
										</div>
									<input class="btreg" type="image" width="100%" src="images/regbt.png" style="position: absolute; bottom: -20px;right: 0;padding: 0;margin: 0;"  /> 
								</div>
								<div class="tms-content bgcab show2 hide-on-mobile" style="background: rgba(45,17,120,0.9);height: 450px; display: none;" >
									<div style="padding: 20px;">
                                   <div style="text-transform: uppercase;font-family: Serif;font-size: 21px;"> REGISTER <span style="color: #fff;font-size: 12px;font-family: sans-serif;">to get the special privilege</span></div>
									<form action="data_post.php" method="post" id="fdes" onsubmit="return checkTheBox();" >
        	<div class="text-box"><input name="name" type="text" placeholder="NAME" required /></div>
            <div class="text-box"><input name="email" type="email"  placeholder="EMAIL" required /></div>
            <div class="text-box"><input name="mobile"  minlength="10" type="text" placeholder="MOBILE" required /></div>
            <div class="text-box">
            รูปแบบห้องที่สนใจ  UNIT INTERESTED
           <br />

            <input name="int[]" 
          type="checkbox" id="int<?=$i?>" value="1 BEDROOM" class="css-checkbox" /><label for="int<?=$i?>" class="css-label">1 BEDROOM
            </label>
           
            <input name="int[]"       type="checkbox" id="int2<?=$i?>" value=" 2 BEDROOMS" class="css-checkbox" /><label for="int2<?=$i?>" class="css-label">2 BEDROOMS
            </label>
            </div>
              <div class="text-box">
           ขนาดห้อง  UNIT SIZE
           <br />

            <input name="size[]"    type="checkbox" id="size<?=$i?>" value="BELOW 35 SQ.M." class="css-checkbox" /><label for="size<?=$i?>" class="css-label">BELOW 35 SQ.M.		   
			   
            </label>
           
            <input name="size[]"    type="checkbox" id="size2<?=$i?>" value="35-50 SQ.M." class="css-checkbox" /><label for="size2<?=$i?>" class="css-label">35-50 SQ.M.
            </label>
            <div class="clear"></div>
            <input name="size[]"  type="checkbox" id="size3<?=$i?>" value="51-65 SQ.M." class="css-checkbox" /><label for="size3<?=$i?>" class="css-label">51-65 SQ.M.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </label>
            <input name="size[]"    type="checkbox" id="size4<?=$i?>" value="ABOVE 66 SQ.M." class="css-checkbox" /><label for="size4<?=$i?>" class="css-label">ABOVE 66 SQ.M.
            </label>
            </div>
            <div class="text-box">
          งบประมาณ  BUDGET
           <br />

            <input name="budget[]"  type="checkbox" id="budget<?=$i?>" value="BELOW 4 MB." class="css-checkbox" />  <label for="budget<?=$i?>" class="css-label">BELOW 4 MB.		
			   
            </label>
           
            <input name="budget[]"  type="checkbox" id="budget2<?=$i?>" value="4-5 MB." class="css-checkbox" />  <label for="budget2<?=$i?>" class="css-label">4-5 MB.
            </label>
             <div class="clear"></div>
            <input name="budget[]"    type="checkbox" id="budget3<?=$i?>" value="5.1-6 MB." class="css-checkbox" />  <label for="budget3<?=$i?>" class="css-label">5.1-6 MB.
            </label>
            <input name="budget[]"    type="checkbox" id="budget4<?=$i?>" value="6.1-8 MB." class="css-checkbox" />  <label for="budget4<?=$i?>" class="css-label">6.1-8 MB.
            </label>
             <div class="clear"></div>
            <input name="budget[]"    type="checkbox" id="budget5<?=$i?>" value="8.1-10 MB." class="css-checkbox" />  <label for="budget5<?=$i?>" class="css-label">8.1-10 MB.
            </label>
            <input name="budget[]"    type="checkbox" id="budget6<?=$i?>" value="ABOVE 10 MB." class="css-checkbox" />  <label for="budget6<?=$i?>" class="css-label">ABOVE 10 MB.
            </label>
            </div></div>
            	<input type="image" width="100%" src="images/regbt.png" style="position: absolute; bottom: -19px;right: 0;padding: 0;margin: 0;"  /> 
						
        
        	<div class="form-response"></div>
									<input type="image" width="100%" src="images/submitbt.png" onclick="" style="position: absolute; bottom: -19px;right: 0;padding: 0;margin: 0;"  /> 
								</form></div>
								<img data-src="<?=baseurl.$rs_pd['path']?>" data-retina src="images/blank.png" alt="<?=baseurl.$rs_pd['alt']?>"/>
							</li>
                   <? $i++;}?>
						</ul>

					</div>
				</section>
				
				<div class="row hide  show-on-mobile full-width" style="margin: 0;">
                            	<div id="mshow" class="column width-12  bgcab2  show1  " style="position: relative;" >
								
									<div style="padding: 20px;<? if($_GET['thank']==1){?>line-height: 30px; <? }?>">
                                    <? if($_GET['thank']==1){?>
                                     <div>ขอบคุณสำหรับข้อมูล<br />ทางทีมงานจะติดต่อกลับท่านโดยเร็วที่สุด</div>
                                    <? }?>
											     <h1 style="color: #fff;" class="ft-db-adama">โลกส่วนตัว</h1>
                                                 <h2 style="color: #fff;" class="ft-db-adama">ที่เชื่อมทุกมุมเมืองให้อยู่ใกล้ๆคุณ</h2>
                                                 <div>ถนนจันทร์-สาทร<br />ราคาเริ่มต้น 3.8 ล้านบาท</div>
										<div style="margin-top: 50px;"><span style="color: #fff;"> จองพร้อมทำสัญญา<br />
รับเฟอร์นิเจอร์แพคเกจ<br /></span>
มูลค่าสูงถึง 350,000 บาท <span style="font-size: 20px;">(จำนวนจำกัด)</span></div></div>
									<input class="btreg" type="image" width="100%" src="images/regbt.png" style="position: absolute; bottom: 0;right: 0;padding: 0;margin: 0;"  /> 
								</div>
								<div id="regm" class="column width-12  bgcab2  show2 hide-on-mobile" style="background: rgba(45,17,120,0.9);height: 600px;display: none;position: relative;" >
									<form action="data_post.php" method="post" id="fdes" onsubmit="return checkTheBox();" >	<div style="padding: 20px;">
                                   <div style="text-transform: uppercase;font-family: Serif;font-size: 21px;"> REGISTER <span style="color: #fff;font-size: 12px;font-family: sans-serif;">to get the special privilege</span></div>
								
        	<div class="text-box"><input name="name" type="text" placeholder="NAME" required /></div>
            <div class="text-box"><input name="email" type="email"  placeholder="EMAIL" required /></div>
            <div class="text-box"><input name="mobile"  minlength="10" type="text" placeholder="MOBILE" required /></div>
            <div class="text-box">
            รูปแบบห้องที่สนใจ  UNIT INTERESTED
           <br />

            <input name="int[]" 
          type="checkbox" id="int<?=$i?>" value="1 BEDROOM" class="css-checkbox" /><label for="int<?=$i?>" class="css-label">1 BEDROOM
            </label>
           
            <input name="int[]"       type="checkbox" id="int2<?=$i?>" value=" 2 BEDROOMS" class="css-checkbox" /><label for="int2<?=$i?>" class="css-label">2 BEDROOMS
            </label>
            </div>
              <div class="text-box">
           ขนาดห้อง  UNIT SIZE
           <br />

            <input name="size[]"    type="checkbox" id="size<?=$i?>" value="BELOW 35 SQ.M." class="css-checkbox" /><label for="size<?=$i?>" class="css-label">BELOW 35 SQ.M.		   
			   
            </label>
           
            <input name="size[]"    type="checkbox" id="size2<?=$i?>" value="35-50 SQ.M." class="css-checkbox" /><label for="size2<?=$i?>" class="css-label">35-50 SQ.M.
            </label>
            <div class="clear"></div>
            <input name="size[]"  type="checkbox" id="size3<?=$i?>" value="51-65 SQ.M." class="css-checkbox" /><label for="size3<?=$i?>" class="css-label">51-65 SQ.M.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </label>
            <input name="size[]"    type="checkbox" id="size4<?=$i?>" value="ABOVE 66 SQ.M." class="css-checkbox" /><label for="size4<?=$i?>" class="css-label">ABOVE 66 SQ.M.
            </label>
            </div>
            <div class="text-box">
          งบประมาณ  BUDGET
           <br />

            <input name="budget[]"  type="checkbox" id="budget<?=$i?>" value="BELOW 4 MB." class="css-checkbox" />  <label for="budget<?=$i?>" class="css-label">BELOW 4 MB.		
			   
            </label>
           
            <input name="budget[]"  type="checkbox" id="budget2<?=$i?>" value="4-5 MB." class="css-checkbox" />  <label for="budget2<?=$i?>" class="css-label">4-5 MB.
            </label>
             <div class="clear"></div>
            <input name="budget[]"    type="checkbox" id="budget3<?=$i?>" value="5.1-6 MB." class="css-checkbox" />  <label for="budget3<?=$i?>" class="css-label">5.1-6 MB.
            </label>
            <input name="budget[]"    type="checkbox" id="budget4<?=$i?>" value="6.1-8 MB." class="css-checkbox" />  <label for="budget4<?=$i?>" class="css-label">6.1-8 MB.
            </label>
             <div class="clear"></div>
            <input name="budget[]"    type="checkbox" id="budget5<?=$i?>" value="8.1-10 MB." class="css-checkbox" />  <label for="budget5<?=$i?>" class="css-label">8.1-10 MB.
            </label>
            <input name="budget[]"    type="checkbox" id="budget6<?=$i?>" value="ABOVE 10 MB." class="css-checkbox" />  <label for="budget6<?=$i?>" class="css-label">ABOVE 10 MB.
            </label>
            </div></div>
            	  <input class="btreg" type="image" width="100%" src="images/submitbt_m.png" style="position: absolute; bottom: 0;right: 0;padding: 0;margin: 0;"  />            
                            
				</form>		
        
        	<div class="form-response"></div>
								
								
                       </div>
							
							</div>

			

				<!-- Portfolio Grid -->
				<div id="gallery" class="section-block grid-container fade-in-progressively full-width bkg-charcoal no-margins no-padding plan" data-layout-mode="masonry" data-grid-ratio="1.5" data-animate-filter-duration="700" data-set-dimensions data-animate-resize data-animate-resize-duration="0">
					<div class="row">
						<div class="column width-12">
							<div class="row grid content-grid-3">
								<div class="grid-item grid-sizer">
									<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="500" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="1">
										<a class="overlay-link lightbox-link" data-toolbar="zoom" data-group="gallery-1" data-caption="Bunker - Berlin, Germany" href="images/02.jpg">
											<img src="images/gal1.jpg" alt=""/>
											<span class="overlay-info">
												<span>
													<span>
														<span class="project-title">Melanie Bangkok</span>
														<span class="project-description">Chan-Sathon</span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
								<div class="grid-item portrait">
									<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="500" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="1">
										<img src="images/gal5.jpg" alt=""/>
									</div>
									<div class="content-outer">
										<div class="content-inner color-white">
											<div class="row">
												<div class="column width-4 offset-4 center">
													<a class="lightbox-link icon-play icon-circled border-white bkg-hover-theme color-white color-hover-white circled medium" data-toolbar="zoom" data-group="gallery-1" data-caption="The Building of Munster" href="http://www.youtube.com/embed/bMhAp2ZSvzM" ></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="grid-item">
									<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="500" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="1">
										<a class="overlay-link lightbox-link" data-toolbar="zoom" data-group="gallery-1" data-caption="Gropius - Madrid, Spain" href="images/07.jpg">
											<img src="images/gal2.jpg" alt=""/>
											<span class="overlay-info">
												<span>
													<span>
													<span class="project-title">Melanie Bangkok</span>
														<span class="project-description">Chan-Sathon</span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
								<div class="grid-item">
									<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="500" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="1">
										<a class="overlay-link lightbox-link" data-toolbar="zoom" data-group="gallery-1" data-caption="Schröder - Stockholm, Sweden" href="images/04.jpg">
											<img src="images/gal3.jpg" alt=""/>
											<span class="overlay-info">
												<span>
													<span>
														<span class="project-title">Melanie Bangkok</span>
														<span class="project-description">Chan-Sathon</span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
								<div class="grid-item">
									<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="500" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="1">
										<a class="overlay-link lightbox-link" data-group="gallery-1" data-caption="Farnsworth - Plano Il, U.S" href="images/01.jpg">
											<img src="images/gal4.jpg" alt=""/>
											<span class="overlay-info">
												<span>
													<span>
														<span class="project-title">Melanie Bangkok</span>
														<span class="project-description">Chan-Sathon</span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="center"><a style="font-family: serif;" href="gallery.php" class="button medium bkg-grey-light  color-white color-hover-white left mt-30 mb-10">View More</a></div>
				</div>
				<!-- Portfolio Grid End -->

			

				<!-- Stat Section 1
				<div class="section-block stats-1 stats-1-1 pt-150 pb-150 center color-white">
					<div class="row">
						<div class="column width-10 offset-1">
							<div class="row content-grid-2">
								<div class="grid-item">
									<div class="stat box no-margin-bottom large bkg-black">
										<div class="stat-inner">
											<p class="counter"><span class="stat-counter" data-count-from="1230" data-count-to="1556">1,230</span> Houses</p>
											<p class="description">Since January 2013</p>
										</div>
									</div>
								</div>
								<div class="grid-item">
									<div class="stat box no-margin-bottom large bkg-black">
										<div class="stat-inner">
											<p class="counter"><span class="stat-counter" data-count-from="230" data-count-to="643">230</span> Clients</p>
											<p class="description">Since January 2013</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				Stat Section 1 -->

				<!-- Team Grid -->
				<div id="news" class="section-block team-2 bkg-charcoal">
					<div class="row horizon" data-animate-in="preset:slideInRightShort;duration:1000ms;" data-threshold="0.2">
						<div class="column width-11 center mb-10 ">
							<h2 class="mb-10" style="color: #ab9767;">NEWS & PROGRESS</h2>
                            <img src="images/line.gif" />
						</div>
						<div class="column width-12">
							<div class="row content-grid-3">
                            <?
                            $option_py = "select * FROM tb_news where Active=1 limit 3";

$query_pd2 = $db->query($option_py);
while($rs_news = $db->get($query_pd2)){
                            ?>
								<div class="grid-item">
									<div class="team-content">
										<div class="thumbnail no-margin-bottom img-scale-in" data-hover-easing="easeInOut" data-hover-speed="500" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="0.9">
											<a class="overlay-link fade-location" href="news_detail.php?id=<?=$rs_news['ID']?>">
												<img src="<?=baseurl.$rs_news['path_crop']?>" alt="team memeber 1" width="760" height="500"/>
												<span class="overlay-info">
													<span>
														<span>
															<span>Read More</span>
														</span>
													</span>
												</span>
											</a>
										</div>
										<div class="team-content-info center">
											<h4 class="mb-5 ft-db-adama" style="color: #ab9767;"><?=$rs_news['title']?></h4>
											<h6 class="occupation mb-20"><?=$rs_news['createDate']?></h6>
											<p><?=$rs_news['description']?></p>
										
										</div>
									</div>
								</div>
                                <? }?>
							
							</div>
						</div>
					</div>
                    <div class="center"><a style="font-family: serif;" href="news.php" class="button medium bkg-grey-light  color-white color-hover-white left mt-30 mb-10">View More</a></div>
				</div>
				<!-- Team Grid End -->

				<!-- Call to Action Section
				<div class="section-block replicable-content pt-60 pb-30 call-to-action-2 call-to-action-2-1 bkg-grey-ultralight background-cover">
					<div class="row">
						<div class="column width-6 offset-3 center color-white">
							<p class="text-uppercase weight-light">Keep Up-To-Date With Sartre<br><a data-content="inline" data-toolbar="" data-networks="twitter;pinterest" data-modal-mode data-modal-width="600" data-lightbox-animation="fade" href="#signup-lightbox" class="lightbox-link color-theme color-hover-white" href="#">Sign Up</a></p>
						</div>
					</div>
				</div>
				Call to Action Section End -->

			
		
			<!-- Content End -->
				<!-- Sign Up Section 2 End -->

				<!-- Signup Modal
				<div id="signup-lightbox" class="signup-2 signup-2-1 pt-70 pb-50 hide">
					<div class="row">
						<div class="column width-12 center">
							<h2 class="mb-30 color-white">Join Our Newsletter:</h2>
							<div class="signup-form-container">
								<form class="signup-form merged-form-elements" action="php/subscribe.php" method="post" novalidate>
									<div class="row">
										<div class="column width-9">
											<div class="field-wrapper">
												<input type="email" name="email" class="form-email form-element" placeholder="Email address*" tabindex="2" required>
											</div>
										</div>
										<div class="column width-3 center">
											<input type="submit" value="Signup" class="form-submit button bkg-theme bkg-hover-charcoal color-white color-hover-white">
										</div>
									</div>
									<input type="text" name="honeypot" class="form-honeypot form-element">
								</form>
								<div class="form-response show color-white"></div>
							</div>
						</div>
					</div>
				</div>
				Signup Modal End -->

			</div>
			<!-- Content End -->

				<!-- Contact Advanced Modal End -->
				<div id="contact-modal" class="section-block pt-0 pb-30 background-none hide">
					
					<!-- Title Section-->
					<div class="section-block pb-0">
						<div class="row">
							<div class="column width-10 offset-1">
								<h1 class="title-medium mb-0">Contact Us</h1>
							</div>
						</div>
					</div>
					<!-- Title Section End -->

					<!-- Form -->
					<div class="section-block pt-60 pb-0">
						<div class="row">
							<div class="column width-10 offset-1">
								<div class="signup-form-container">
									<form class="contact-form" action="php/send-email.php" method="post" novalidate>
										<div class="row">
											<div class="column width-12">
												<input type="text" name="fname" class="form-fname form-element large" placeholder="First Name*" tabindex="1" required>
											</div>
											<div class="column width-12">
												<input type="email" name="email" class="form-email form-element large" placeholder="Email address*" tabindex="3" required>
											</div>
											<div class="column width-6">
												<input type="text" name="honeypot" class="form-honeypot form-element large">
											</div>
										</div>
										<div class="row">
											<div class="column width-12">
												<div class="field-wrapper">
													<textarea name="message" class="form-message form-element large" placeholder="Message*" tabindex="7" required></textarea>
												</div>
											</div>
											<div class="column width-12">
												<input type="submit" value="Send Email" class="form-submit button medium bkg-theme bkg-hover-theme color-white color-hover-white">
											</div>
										</div>
									</form>
									<div class="form-response center"></div>
								</div>
							</div>
						</div>
					</div>
					<!-- Form End -->
				</div>
			<!-- Footer -->
			<footer style="background:#f5f5f5;font-family: rsulight;font-size: 23px;color:#ac9766;">
	<div class="container center">
   
    CALL 092 925 2555
    <a style="color:#ac9766;" href="mailto:info@primegatesrealty.com"><i class="icon-mail" aria-hidden="true"></i><span style="font-family: sans-serif;font-size: 14px;"> Email Us</span></a>
    <a target="_blank" href="https://www.facebook.com/melaniebangkok/" style="color:#ac9766;"><i class="icon-facebook" aria-hidden="true"></i></a>
    <a target="_blank" href="https://www.instagram.com/melanie_bangkok/" style="color:#ac9766;"><i class="icon-instagram" aria-hidden="true"></i></a>


     <img src="images/images/footer_05.jpg" />
    <img src="images/images/footer_06.jpg" /> 
    </div>
  
</footer>
			<!-- Footer End -->

		</div>
	</div>

	<!-- Js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maps.googleapis.com/maps/api/js?v=3"></script>
	<script src="js/timber.master.min.js"></script>
    <script type="text/javascript">
    $(function() {
    $(".btreg")
        .on( 'click', function(){
            var slider = $( '#homeslide' ).data( 'avalancheSlider' );
            slider.pauseSlideshow();
            $('.show1') .fadeOut(400);
            $('.show2')  .fadeIn(400);
           $('#mshow').addClass('hide-on-mobile');

$('#regm').removeClass('hide-on-mobile');
$('#regm').addClass('show-on-mobile');
            
        });
        
});
    function checkTheBox(){
   var chk_arr =  document.getElementsByName("int[]");
    var chk_arr2 =  document.getElementsByName("size[]");
    var chk_arr3 =  document.getElementsByName("budget[]");
var chklength = chk_arr.length;
var chklength2 = chk_arr2.length;
var chklength3 = chk_arr3.length;
var inters =0;
var sizei =0;
var bg =0;
        for(k=0;k< chklength;k++)
        {
            if(chk_arr[k].checked){
                inters++;
            }
        } 
        for(k=0;k< chklength2;k++)
        {
            if(chk_arr2[k].checked){
                sizei++;
            }
        } 
        for(k=0;k< chklength3;k++)
        {
            if(chk_arr3[k].checked){
                bg++;
            }
        } 
  if(inters<1){
    return false;
  }
   if(sizei<1){
    return false;
  }
  if(bg<1){
    return false;
  }   
}
	
</script>
  <script>
       (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 

  ga('create', 'UA-85233516-1', 'auto');

  ga('send', 'pageview');
  
  
      
      </script>  
      <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
 
fbq('init', '1169958919733715'); // Insert your pixel ID here. 
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1169958919733715&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->       
</body>
</html>