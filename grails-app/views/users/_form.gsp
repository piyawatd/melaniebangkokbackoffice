<g:if test="${flash.error}">
    <div class="alert alert-danger" role="alert">
        ${flash.error}
        <g:eachError bean="${users}">
            <li>${it}</li>
        </g:eachError>
    </div>
</g:if>
<div class="row">
    <div class="form-group">
        <label for="inputUsername" class="col-sm-2 control-label">Username</label>
        <div class="col-sm-10">
            <input type="text" name="username" class="form-control" id="inputUsername" placeholder="username" value="${users.getUsername()}" required <g:if test="${params.action == 'editForm' || params.action == 'update'}">readonly</g:if>>
        </div>
    </div>
    <div class="form-group">
        <label for="inputPassword" class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
            <input type="password" name="password" class="form-control" id="inputPassword">
        </div>
    </div>
    <div class="form-group">
        <label for="inputFirstname" class="col-sm-2 control-label">Firstname</label>
        <div class="col-sm-10">
            <input type="text" name="firstName" class="form-control" id="inputFirstname" placeholder="firstname" value="${users.getFirstName()}">
        </div>
    </div>
    <div class="form-group">
        <label for="inputLastname" class="col-sm-2 control-label">Lastname</label>
        <div class="col-sm-10">
            <input type="text" name="lastName" class="form-control" id="inputLastname" placeholder="lastname" value="${users.getLastName()}">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" name="email" class="form-control" id="inputEmail" placeholder="email" value="${users.getEmail()}">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail" class="col-sm-2 control-label">User Level</label>
        <div class="col-sm-10">
            <select class="form-control" name="userLevel">
                <option value="0" <g:if test="${session.users.getUserLevel() == 0}">selected="selected"</g:if>>user</option>
                <g:if test="${session.users.getUserLevel() == 2}">
                    <option value="1" <g:if test="${session.users.getUserLevel() == 1}">selected="selected"</g:if>>admin</option>
                    <option value="2" <g:if test="${session.users.getUserLevel() == 2}">selected="selected"</g:if>>saadmin</option>
                </g:if>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Save</button>
            <a href="<g:createLink action="index"/>" class="btn btn-default">Cancel</a>
        </div>
    </div>
</div>