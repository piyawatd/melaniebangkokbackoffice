<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>User : New</title>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/users">User</a></li>
            <li class="active">User : New</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="no-margin-top">User : New</h1>
        </div>
    </div>
    <g:form action="create" method="POST" class="form-horizontal">
        <g:render template="form" model="['users':users]"/>
    </g:form>
</div>
</body>
</html>
