<!doctype html>
<html>
<head>
    <meta name="layout" content="login"/>
    <title>Login</title>
</head>
<body>
<style>
body {
    padding-top: 50px;
}
.starter-template {
    padding: 40px 15px;
}
.alert {
    margin-top: 40px;
}
</style>
<div class="container">
    <g:if test="${flash.error}">
        <div class="alert alert-danger" role="alert">
            ${flash.error}
        </div>
    </g:if>
    <g:if test="${flash.message}">
        <div class="alert alert-success" role="alert">
            ${flash.message}
        </div>
    </g:if>
    <div class="starter-template">
        <form class="form-horizontal" action="<g:createLink action="login"/>" method="POST">
            <div class="form-group">
                <label for="inputUsername" class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="username" id="inputUsername" placeholder="username">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" name="password" id="inputPassword" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Sign in</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
</script>
</body>
</html>
