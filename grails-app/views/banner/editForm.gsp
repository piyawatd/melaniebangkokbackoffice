<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>Banner : ${banner.getTitleTh()}</title>
    <asset:stylesheet src="tabstyle.css"/>
    <ckeditor:resources/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/banner">Frontpage Banner</a></li>
            <li class="active">Frontpage Banner : ${banner.getTitleTh()}</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="no-margin-top">Frontpage Banner : ${banner.getTitleTh()}</h1>
        </div>
    </div>
    <g:form action="update" method="POST" id="${banner.getId()}" class="form-horizontal">
        <g:render template="form" model="['banner':banner]"/>
    </g:form>
</div>
</body>
</html>
