<div class="form-group">
    <label for="inputTitleEn" class="col-sm-2 control-label">Title</label>
    <div class="col-sm-10">
        <input type="text" name="titleEn" class="form-control" id="inputTitleEn" placeholder="title"
               value="${banner.getTitleEn()}">
    </div>
</div>
<div class="form-group">
    <label for="inputCaptionEn" class="col-sm-2 control-label">Caption</label>
    <div class="col-sm-10">
        <textarea name="captionEn" class="form-control"
                  id="inputCaptionEn">${banner.getCaptionEn() ?: ""}</textarea>
    </div>
</div>
<!--- File Upload --->
<div class="form-group">
    <label for="imageEn" class="col-sm-2 control-label">File Upload</label>

    <div class="col-sm-10">
        <input type="text" value="" class="form-control" style="width:200px;float:left;margin-right:10px"
               readonly="readonly" name="imageEn" id="imageEn">
        <input type="button" value="Browse" onclick="showDialog('en')" class="btn btn-default"/>
        <p class="help-block">ขนาดรูปภาพ 1533 * 1023 px</p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Thumbnail</label>

    <div class="col-sm-10">
        <ul class="thumbnails">
            <g:if test="${banner.getImageEn() && banner.getImageEn() != ""}">
                <li class="col-md-3">
                    <div class="thumbnail">
                        <a class="glyphicon glyphicon-zoom-in pull-right" href="javascript:previewimage('<g:resource dir="uploadresource/banner/" file="${banner.getImageEn()}"/>')"></a>
                        <a href="javascript:previewimage('<g:resource dir="uploadresource/banner/" file="${banner.getImageEn()}"/>')">
                            <img src="<g:resource dir="uploadresource/banner/" file="${banner.getImageEn()}"/>" class="img-thumb img-rounded"/>
                        </a>
                        <p>Current Thumbnail</p>
                    </div>
                </li>
            </g:if>
            <li class="span3" id="previewthumben">

            </li>
        </ul>
    </div>
</div>