<div class="form-group">
    <label for="inputTitleCh" class="col-sm-2 control-label">Title</label>

    <div class="col-sm-10">
        <input type="text" name="titleCh" class="form-control" id="inputTitleCh" placeholder="title"
               value="${banner.getTitleCh()}">
    </div>
</div>

<div class="form-group">
    <label for="inputCaptionCh" class="col-sm-2 control-label">Caption</label>

    <div class="col-sm-10">
        <textarea name="captionCh" class="form-control"
                  id="inputCaptionCh">${banner.getCaptionCh() ?: ""}</textarea>
    </div>
</div>
<!--- File Upload --->
<div class="form-group">
    <label for="imageCh" class="col-sm-2 control-label">File Upload</label>

    <div class="col-sm-10">
        <input type="text" value="" class="form-control" style="width:200px;float:left;margin-right:10px"
               readonly="readonly" name="imageCh" id="imageCh">
        <input type="button" value="Browse" onclick="showDialog('ch')" class="btn btn-default"/>
        <p class="help-block">ขนาดรูปภาพ 1533 * 1023 px</p>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Thumbnail</label>

    <div class="col-sm-10">
        <ul class="thumbnails">
            <g:if test="${banner.getImageEn() && banner.getImageEn() != ""}">
                <li class="col-md-3">
                    <div class="thumbnail">
                        <a class="glyphicon glyphicon-zoom-in pull-right" href="javascript:previewimage('<g:resource dir="uploadresource/banner/" file="${banner.getImageCh()}"/>')"></a>
                        <a href="javascript:previewimage('<g:resource dir="uploadresource/banner/" file="${banner.getImageCh()}"/>')">
                            <img src="<g:resource dir="uploadresource/banner/" file="${banner.getImageCh()}"/>" class="img-thumb img-rounded"/>
                        </a>
                        <p>Current Thumbnail</p>
                    </div>
                </li>
            </g:if>
            <li class="span3" id="previewthumbch">

            </li>
        </ul>
    </div>
</div>