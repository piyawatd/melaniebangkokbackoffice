<div class="form-group">
    <label for="inputTitleTh" class="col-sm-2 control-label">Title</label>
    <div class="col-sm-10">
        <input type="text" name="titleTh" class="form-control" id="inputTitleTh" placeholder="title" value="${banner.getTitleTh()}" required>
    </div>
</div>
<div class="form-group">
    <label for="inputCaptionTh" class="col-sm-2 control-label">Caption</label>
    <div class="col-sm-10">
        <textarea name="captionTh" class="form-control"
                  id="inputCaptionTh">${banner.getCaptionTh() ?: ""}</textarea>
    </div>
</div>
<!--- File Upload --->
<div class="form-group">
    <label for="imageTh" class="col-sm-2 control-label">File Upload</label>

    <div class="col-sm-10">
        <input type="text" value="" class="form-control" style="width:200px;float:left;margin-right:10px"
               readonly="readonly" name="imageTh" id="imageTh">
        <input type="button" value="Browse" onclick="showDialog('th')" class="btn btn-default"/>
        <p class="help-block">ขนาดรูปภาพ 1533 * 1023 px</p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Thumbnail</label>

    <div class="col-sm-10">
        <ul class="thumbnails">

            <g:if test="${banner.getImageTh() && banner.getImageTh() != ""}">
                <li class="col-md-3">
                    <div class="thumbnail">
                        <a class="glyphicon glyphicon-zoom-in pull-right" href="javascript:previewimage('<g:resource dir="uploadresource/banner/" file="${banner.getImageTh()}"/>')"></a>
                        <a href="javascript:previewimage('<g:resource dir="uploadresource/banner/" file="${banner.getImageTh()}"/>')">
                            <img src="<g:resource dir="uploadresource/banner/" file="${banner.getImageTh()}"/>" class="img-thumb img-rounded"/>
                        </a>
                        <p>Current Thumbnail</p>
                    </div>
                </li>
            </g:if>

            <li class="col-md-3" id="previewthumbth">

            </li>
        </ul>
    </div>
</div>