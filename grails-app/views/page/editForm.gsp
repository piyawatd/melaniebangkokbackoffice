<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>Page : ${page.getTitleTh()}</title>
    <asset:stylesheet src="tabstyle.css"/>
    <ckeditor:resources/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/page">Page</a></li>
            <li class="active">News : ${page.getTitleTh()}</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="no-margin-top">Page : ${page.getTitleTh()}</h1>
        </div>
    </div>
    <g:form action="update" method="POST" id="${page.getId()}" class="form-horizontal">
        <g:render template="form" model="['page':page]"/>
    </g:form>
</div>
</body>
</html>
