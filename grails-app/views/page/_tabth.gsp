<div class="form-group">
    <label for="inputTitleTh" class="col-sm-2 control-label">Title</label>
    <div class="col-sm-10">
        <input type="text" name="titleTh" class="form-control" id="inputTitleTh" placeholder="title" value="${page.getTitleTh()}" required>
    </div>
</div>

<div class="form-group">
    <label for="inputDetailTh" class="col-sm-2 control-label">Detail</label>

    <div class="col-sm-10">
        <ckeditor:editor name="detailTh" id="inputDetailTh" height="1000px" width="100%" toolbar="Mytoolbar">
            ${page.getDetailTh()?: ""}
        </ckeditor:editor>
    </div>
</div>
