<div class="form-group">
    <label for="inputTitleCh" class="col-sm-2 control-label">Title</label>

    <div class="col-sm-10">
        <input type="text" name="titleCh" class="form-control" id="inputTitleCh" placeholder="title"
               value="${page.getTitleCh()}" required>
    </div>
</div>


<div class="form-group">
    <label for="inputDetailCh" class="col-sm-2 control-label">Detail</label>

    <div class="col-sm-10">
        <ckeditor:editor name="detailCh" id="inputDetailCh" height="1000px" width="100%" toolbar="Mytoolbar">
            ${page.getDetailCh() ?: ""}
        </ckeditor:editor>
    </div>
</div>
