<div class="row">
    <div class="form-group">
        <label for="inputFirstname" class="col-sm-2 control-label">Firstname</label>
        <div class="col-sm-10">
            <input type="text" name="firstName" class="form-control" id="inputFirstname" placeholder="firstname" value="${register.getFirstName()}">
        </div>
    </div>
    <div class="form-group">
        <label for="inputLastname" class="col-sm-2 control-label">Lastname</label>
        <div class="col-sm-10">
            <input type="text" name="lastName" class="form-control" id="inputLastname" placeholder="lastname" value="${register.getLastName()}">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="text" name="email" class="form-control" id="inputEmail" placeholder="email" value="${register.getEmail()}">
        </div>
    </div>
    <div class="form-group">
        <label for="inputTel" class="col-sm-2 control-label">Telephone</label>
        <div class="col-sm-10">
            <input type="text" name="tel" class="form-control" id="inputTel" placeholder="tel" value="${register.getTel()}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Price Range</label>
        <div class="col-sm-10">
            <select name="priceRange">
                <g:each in="${priceRange.keySet()}">
                    <option value="${it}" <g:if test="${it == register.getPriceRange()}">selected</g:if>>${priceRange.get(it)}</option>
                </g:each>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Unit Type</label>
        <div class="col-sm-10">
            <select name="unitType">
                <g:each in="${unitType.keySet()}">
                    <option value="${it}" <g:if test="${it == register.getUnitType()}">selected</g:if>>${unitType.get(it)}</option>
                </g:each>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Unit Size</label>
        <div class="col-sm-10">
            <select name="unitSize">
                <g:each in="${unitSize.keySet()}">
                    <option value="${it}" <g:if test="${it == register.getUnitSize()}">selected</g:if>>${unitSize.get(it)}</option>
                </g:each>
            </select>
            %{--<input type="text" name="unitsize" class="form-control" id="inputUnitSize" placeholder="unit size" value="${unitSizeValue}">--}%
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Goal</label>
        <div class="col-sm-10">
            <select name="unitSize">
                <g:each in="${goal.keySet()}">
                    <option value="${it}" <g:if test="${it == register.getGoal()}">selected</g:if>>${goal.get(it)}</option>
                </g:each>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="<g:createLink action="index"/>" class="btn btn-default">Cancel</a>
        </div>
    </div>
</div>