<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>Gallery : ${gallery.getTitleTh()}</title>
    <asset:stylesheet src="tabstyle.css"/>
    <asset:stylesheet src="ui-sortable.css"/>
    <asset:javascript src="jquery-ui.js"/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/gallery">Gallery</a></li>
            <li class="active">Gallery : ${gallery.getTitleTh()}</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="no-margin-top">Gallery : ${gallery.getTitleTh()}</h1>
        </div>
    </div>
    <g:form action="update" method="POST" id="${gallery.getId()}" class="form-horizontal">
        <g:render template="form" model="['gallery':gallery,'galleryImages':galleryImages]"/>
    </g:form>
</div>
<script>
    $( function() {
        $( "#sortable" ).sortable({
            placeholder: "ui-state-highlight"
        });
        $('#saveSort').click( function(e) {
            saveSort();
            return false;
        });
    })

    function alertDel(id,message) {
        if (confirm('Are you sure you want to delete '+message+'?')) {
            $.ajax({
                url: '/gallery/deleteImage/'+id,
                method: "POST",
                dataType: "json"
            }).done(function(result){
                if(result.status)
                {
                    alert("Delete Complete.")
                    $('.gallery-image-'+id).remove()
                }else{
                    alert(result.message)
                }
            })
        }
    }

    function saveSort()
    {
        var sortList = []
        $("#sortable").find("li").each(function() {
            sortList.push($(this).attr("id"))
        })
        $.ajax({
            url: '/gallery/saveSort',
            method: "POST",
            data: { sortList : sortList.join(", ")},
            dataType: "json"
        }).done(function(result){
            if(result.status)
            {
                alert("Save Complete.")
            }
        })
    }
</script>
</body>
</html>
