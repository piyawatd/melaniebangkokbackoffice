<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>Gallery Image : New</title>
    <asset:stylesheet src="tabstyle.css"/>
    <ckeditor:resources/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/gallery">Gallery</a></li>
            <li><a href="<g:createLink action="editForm" id="${gallery.getId()}"/>">${gallery.getTitleTh()}</a></li>
            <li class="active">Gallery Image : New</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="no-margin-top">Gallery Image</h1>
        </div>
    </div>
    <g:form action="createImage" method="POST" id="${gallery.getId()}" class="form-horizontal">
        <g:render template="formImage" model="['galleryImage':galleryImage]"/>
    </g:form>
</div>
</body>
</html>
