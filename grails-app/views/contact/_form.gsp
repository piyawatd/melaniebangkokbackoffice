<div class="row">
    <div class="form-group">
        <label for="inputName" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input type="text" name="name" class="form-control" id="inputName" placeholder="name" value="${contact.getName()}">
        </div>
    </div>

    <div class="form-group">
        <label for="inputEmail" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" name="email" class="form-control" id="inputEmail" placeholder="email" value="${contact.getEmail()}">
        </div>
    </div>

    <div class="form-group">
        <label for="inputPhone" class="col-sm-2 control-label">Mobile Phone</label>
        <div class="col-sm-10">
            <input type="text" name="phone" class="form-control" id="inputPhone" placeholder="phone" value="${contact.getPhone()}">
        </div>
    </div>

    <div class="form-group">
        <label for="inputMessage" class="col-sm-2 control-label">Message</label>
        <div class="col-sm-10">
            <input type="text" name="message" class="form-control" id="inputMessage" placeholder="message" value="${contact.getMessage()}">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="<g:createLink action="index"/>" class="btn btn-default">Cancel</a>
        </div>
    </div>
</div>