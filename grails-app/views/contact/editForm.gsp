<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>Contact : ${contact.getName()}</title>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/users">contact</a></li>
            <li class="active">Contact : ${contact.getName()}</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="no-margin-top">Contact : ${contact.getName()}</h1>
        </div>
    </div>
    <g:form action="update" method="POST" id="${contact.getId()}" class="form-horizontal">
        <g:render template="form" model="['contact':contact]"/>
    </g:form>
</div>
</body>
</html>
