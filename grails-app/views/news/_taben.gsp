<div class="form-group">
    <label for="inputTitleEn" class="col-sm-2 control-label">Title</label>
    <div class="col-sm-10">
        <input type="text" name="titleEn" class="form-control" id="inputTitleEn" placeholder="title"
               value="${news.getTitleEn()}">
    </div>
</div>
<div class="form-group">
    <label for="inputDescriptionEn" class="col-sm-2 control-label">Description</label>
    <div class="col-sm-10">
        <textarea name="descriptionEn" class="form-control"
                  id="inputDescriptionEn">${news.getDescriptionEn() ?: ""}</textarea>
    </div>
</div>
<div class="form-group">
    <label for="inputDetailEn" class="col-sm-2 control-label">Detail</label>

    <div class="col-sm-10">
        <ckeditor:editor name="detailEn" id="inputDetailEn" height="600px" width="100%" toolbar="Mytoolbar">
            ${news.getDetailEn() ?: ""}
        </ckeditor:editor>
    </div>
</div>
<!--- File Upload --->
<div class="form-group">
    <label for="imageEn" class="col-sm-2 control-label">File Upload</label>

    <div class="col-sm-10">
        <input type="text" value="" class="form-control" style="width:200px;float:left;margin-right:10px"
               readonly="readonly" name="imageEn" id="imageEn">
        <input type="button" value="Browse" onclick="showDialog('en')" class="btn btn-default"/>
        <p class="help-block">ขนาดรูปภาพ 1533 * 1023 px</p>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Thumbnail</label>

    <div class="col-sm-10">
        <ul class="thumbnails">
            <g:if test="${news.getImageTh() && news.getImageTh() != ""}">
                <li class="col-md-3 li-imageEn">
                    <div class="thumbnail">
                        <a class="glyphicon glyphicon-trash pull-right-margin-right" href="javascript:deleteimage('imageEn')"></a>
                        <a class="glyphicon glyphicon-zoom-in pull-right" href="javascript:previewimage('<g:resource dir="uploadresource/news/" file="${news.getImageEn()}"/>')"></a>
                        <a href="javascript:previewimage('<g:resource dir="uploadresource/news/" file="${news.getImageEn()}"/>')">
                            <img src="<g:resource dir="uploadresource/news/" file="${news.getImageEn()}"/>" class="img-thumb img-rounded"/>
                        </a>
                        <p>Current Thumbnail</p>
                    </div>
                </li>
            </g:if>
            <li class="col-md-3" id="previewthumben">

            </li>
        </ul>
    </div>
</div>