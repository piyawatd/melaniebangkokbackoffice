<g:if test="${flash.error}">
    <div class="alert alert-danger" role="alert">
        <g:eachError bean="${news}">
            <li>${it}</li>
        </g:eachError>
    </div>
</g:if>
<div class="row">
    <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#th" aria-controls="th" role="tab" data-toggle="tab">Thai</a></li>
            <li role="presentation"><a href="#en" aria-controls="en" role="tab" data-toggle="tab">English</a></li>
            <li role="presentation"><a href="#ch" aria-controls="ch" role="tab" data-toggle="tab">Chinese</a></li>
        </ul>
        <!-- Ck Custom Toolbar -->
        <ckeditor:config var="toolbar_Mytoolbar">
            [
                [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo'],[ 'Maximize'],
                [ 'Styles','Format'],[ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote'],
                [ 'Source', '-', 'Bold', 'Italic','Strike','-','RemoveFormat' ],
                [ 'Image','Table','HorizontalRule','SpecialChar'],['About']
            ]
        </ckeditor:config>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="th">
                <g:render template="tabth" model="[news:news]"/>
            </div>
            <div role="tabpanel" class="tab-pane" id="en">
                <g:render template="taben" model="[news:news]"/>
            </div>
            <div role="tabpanel" class="tab-pane" id="ch">
                <g:render template="tabch" model="[news:news]"/>
            </div>
        </div>

    </div>

    <div class="form-group">
        <div class="col-sm-10 col-md-offset-2">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="active" id="active" value="true" <g:if test="${news.getActive() == true}">checked</g:if>>
                    Active
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="<g:createLink action="index"/>" class="btn btn-default">Cancel</a>
        </div>
    </div>
</div>
<!---    Preview Thumb--->
<div class="modal fade" role="dialog" id="pModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Preview Image</h3>
            </div>
            <div class="modal-body" id="previewimage">

            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
    </div>
</div>
<!---    Upload--->
<div class="modal fade" tabindex="-1" role="dialog" id="bModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Upload Image</h4>
            </div>

            <div class="modal-body">
                <div id="resultupload"></div>

                <div id="queue"></div>
                <input id="file_upload" name="file_upload" type="file">
            </div>

            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                <a onclick="doupload()" class="btn btn-default">Upload</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    var fileLanguage = ""
    $(function () {
        $('#file_upload').uploadifive({
            'auto': false,
            'fileSizeLimit': 50000,
            'multi': false,
            'height': 25,
            'queueID': 'queue',
            'uploadScript': '<g:createLink controller="upload" action="image"/>?format=json&fileType=image&module=news',
            'queueSizeLimit': 1,
            'onUploadComplete': function (file, data) {
                var jsonobj = $.parseJSON(data);
                if(jsonobj.status)
                {
                    $('#resultupload').html('Upload Complete');
                    upfilecomplete(jsonobj);
                }else{
                    $('#queue').html('');
                    $('#resultupload').html('Invalid File Type');
                    $('#file_upload').uploadifive('clearQueue');
                }
            }
        });
    });

    function upfilecomplete(data) {
        if(fileLanguage == 'th')
        {
            $('#imageTh').val(data.filename);
            $('#previewthumbth').html('<a class="thumbnail"><img src="' + data.pathfile + '"/></a>');
        }else if(fileLanguage == 'en')
        {
            $('#imageEn').val(data.filename);
            $('#previewthumben').html('<a class="thumbnail"><img src="' + data.pathfile + '"/></a>');
        }else{
            $('#imageCh').val(data.filename);
            $('#previewthumbch').html('<a class="thumbnail"><img src="' + data.pathfile + '"/></a>');
        }
        $("#bModal").modal('hide');
    }

    function doupload() {
        $('#file_upload').uploadifive('upload')
    }

    function showDialog(pFileLanguage) {
        fileLanguage = pFileLanguage
        $('#resultupload').html('');
        $('#file_upload').uploadifive('clearQueue');
        $("#modal-upload-iframe").html('<iframe id="modalIframeId" width="530" height="141" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" />');
        $('#bModal').modal('show');
    }

    function previewimage(imagename)
    {
    var pathpreview = '<div class="thumbnail"><img src="'+imagename+'"/></div>';
    $('#previewimage').html(pathpreview);
        $('#pModal').modal('show');
    }
    $('#pModal').on('hidden', function () {
        $("#previewimage").html('');
    });

    function deleteimage(pInputName)
    {
        $('#'+pInputName).val("delete")
        $('.li-'+pInputName).remove()
    }
</script>