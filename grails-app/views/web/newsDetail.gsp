<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
    <meta name="twitter:widgets:theme" content="light">
    <meta property="og:title" content="Melanie Bangkok ถนนจันทร์-สาทร"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="http://www.melaniebangkok.com/images/slide3.jpg"/>
    <meta property="og:description" content="Melanie Bangkok ถนนจันทร์-สาทร"/>
    <title>Melanie Bangkok ถนนจันทร์-สาทร</title>
    <link rel="shortcut icon" type="image/x-icon" href="/assets/front/theme-mountain-favicon.ico">



    <!-- Css -->
    <g:if test="${session.lang == "Th"}">
        <asset:stylesheet src="front/thai-fonts.css"/>
    </g:if>
    <g:if test="${session.lang == "En"} ${session.lang == "Ch"}">
        <asset:stylesheet src="front/eng-fonts.css"/>
    </g:if>
    <asset:stylesheet src="front/core.min.css"/>
    <asset:stylesheet src="front/skin-architecture.css"/>
    <asset:stylesheet src="front/header.css"/>
    <asset:stylesheet src="front/index.css"/>
    <asset:stylesheet src="front/news_detail.css"/>
    <asset:stylesheet src="front/footer.css"/>

</head>

<body class="shop home-page">

<g:render template="headerCollapse"/>

<div class="wrapper reveal-side-navigation">
    <div class="wrapper-inner">

        <!-- Header -->
        <header class="header header-fixed header-top header-fixed-on-mobile header-transparent" data-sticky-threshold="window-height" data-bkg-threshold="100">
            <div class="header-inner">
                <div class="row nav-bar clear-row-mg">
                    <div class="column width-12 nav-bar-inner">
                        <div class="logo">
                            <div class="logo-inner">
                                <a href="<g:createLink action="index"/>"><img src="/assets/front/logo.png" alt="melanie Logo"/></a>
                                <a href="<g:createLink action="index"/>"><img src="/assets/front/logo.png" alt="melanie Logo"/></a>
                            </div>
                        </div>
                        <div>
                            <nav class="navigation nav-block secondary-navigation nav-right">
                                <ul>
                                    <li class="aux-navigation hide">
                                        <!-- Aux Navigation -->
                                        <a href="#" class="navigation-show side-nav-show nav-icon">
                                            <span class="icon-menu"></span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="pull-right language-key">
                            <a href="<g:createLink action="newsDetail" params="[lang:'Th']" id="${news.getId()}"/>"><strong>TH</strong></a> |
                            <a href="<g:createLink action="newsDetail" params="[lang:'En']" id="${news.getId()}"/>"><strong>EN</strong></a>
                            | <a href="<g:createLink action="newsDetail" params="[lang:'zh_Cn']" id="${news.getId()}"/>"><strong>中文</strong></a>
                        </div>
                        <nav class="navigation nav-block primary-navigation nav-right no-margin-right link-hilight">
                            <g:render template="header"/>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header End -->


        <!-- Content -->
        <div class="content clearfix">

            <!-- Intro Title Section 2 -->
            <div class="section-block intro-title-2 intro-title-2-4 news-head">
                <img src="<g:resource dir="uploadresource/news/" file="${news["image${lang}"]}"/>">
            </div>
            <!-- Intro Title Section 2 End -->

            <div class="section-block clearfix no-padding mt-30">

                <div class="row">

                    <!-- Content Inner -->
                    <div class="column width-9 content-inner blog-single-post">
                        <h3 class="inline news-title2">${news["title${lang}"]}</h3>
                        <article class="post">
                            <div class="post-content">
                                ${raw(news["detail${lang}"])}
                            </div>
                        </article>
                    </div>
                    <!-- Content Inner End -->

                    <!-- Sidebar -->
                    <aside class="column width-3 sidebar right">

                        <div class="widget">
                            <h3 class="widget-title">Recent Posts</h3>
                            <ul class="list-group">
                                <g:each in="${newsLists}">
                                    <li>
                                        <span class="post-info"></span>
                                        <a href="<g:createLink action="newsDetail"
                                                               id="${it.getId()}"/>">${it["title${lang}"]}</a>
                                    </li>
                                </g:each>
                            </ul>
                        </div>

                        <div class="widget">
                            <h3 class="widget-title">Tweets</h3>
                            <!-- twitter -->
                            <a class="twitter-timeline" href="https://twitter.com/melaniebangkok">Tweets by
                            melaniebangkok</a>
                            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                    </aside>
                </div>

                <!-- Sidebar End -->

            </div>

        </div>
        <!-- Content End -->

        <g:render template="footer"/>

    </div>
</div>

<!-- Js -->
<asset:javascript src="front/jquery-3.1.1.min.js"/>
<asset:javascript src="front/timber.master.min.js"/>
</body>
</html>