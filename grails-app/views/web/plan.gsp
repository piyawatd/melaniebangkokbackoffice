<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
    <meta name="format-detection" content="telephone=no"/>
    <meta name="twitter:widgets:theme" content="light">
    <meta property="og:title" content="Melanie Bangkok ถนนจันทร์-สาทร"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="http://www.melaniebangkok.com/images/slide3.jpg"/>
    <meta property="og:description" content="Melanie Bangkok ถนนจันทร์-สาทร"/>
    <title>Melanie Bangkok ถนนจันทร์-สาทร</title>
    <link rel="shortcut icon" type="image/x-icon" href="/assets/front/theme-mountain-favicon.ico">



    <!-- Css -->
    <g:if test="${session.lang == "Th"}">
        <asset:stylesheet src="front/thai-fonts.css"/>
    </g:if>
    <g:if test="${session.lang == "En"} ${session.lang == "Ch"}">
        <asset:stylesheet src="front/eng-fonts.css"/>
    </g:if>
    <asset:stylesheet src="front/core.min.css"/>
    <asset:stylesheet src="front/skin-architecture.css"/>
    <asset:stylesheet src="front/header.css"/>
    <asset:stylesheet src="front/index.css"/>
    <asset:stylesheet src="front/project.css"/>
    <asset:stylesheet src="front/unit.css"/>
    <asset:stylesheet src="front/footer.css"/>
    <asset:stylesheet src="jquery.bxslider/jquery.bxslider.css"/>


</head>

<body class="shop home-page">

<g:render template="headerCollapse"/>

<div class="wrapper reveal-side-navigation">
    <div class="wrapper-inner">

        <!-- Header -->
        <header class="header header-fixed header-top header-fixed-on-mobile header-transparent" data-sticky-threshold="window-height" data-bkg-threshold="100">
            <div class="header-inner">
                <div class="row nav-bar clear-row-mg">
                    <div class="column width-12 nav-bar-inner">
                        <div class="logo">
                            <div class="logo-inner">
                                <a href="<g:createLink action="index"/>"><img src="/assets/front/logo.png" alt="melanie Logo"/></a>
                                <a href="<g:createLink action="index"/>"><img src="/assets/front/logo.png" alt="melanie Logo"/></a>
                            </div>
                        </div>
                        <div>
                            <nav class="navigation nav-block secondary-navigation nav-right">
                                <ul>
                                    <li class="aux-navigation hide">
                                        <!-- Aux Navigation -->
                                        <a href="#" class="navigation-show side-nav-show nav-icon">
                                            <span class="icon-menu"></span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="pull-right language-key">
                            <a href="<g:createLink action="plan" params="[lang:'Th']"/>"><strong>TH</strong></a> |
                            <a href="<g:createLink action="plan" params="[lang:'En']"/>"><strong>EN</strong></a>
                            | <a href="<g:createLink action="plan" params="[lang:'zh_Cn']"/>"><strong>中文</strong></a>
                        </div>
                        <nav class="navigation nav-block primary-navigation nav-right no-margin-right link-hilight">
                            <g:render template="header"/>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header End -->


        <!-- Content -->
        <div class="content clearfix">

        <!--Banner-->
        <div class="project-banner">
            <img src="/assets/front/planimg.jpg" class="hide-on-mobile show-on-mobile3">
            <img src="/assets/front/planimg@2x.jpg" class="hide show-on-mobile">
            <div class="project">
                <div class="project-menu">
                    <p><g:message code="plan.topic"/></p>
                    <ul>
                        <li><a href="javascript:void(0);" id="slide-to1"><g:message code="plan.slide1"/></a></li>
                        <li><a href="javascript:void(0);" id="slide-to2"><g:message code="plan.slide2"/></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!--Banner End-->


        <!--slide-->
        <div class="section-block plan no-padding ">
            <ul class="bxslider">
                <li>
                    <div class="row no-margins">
                        <div class="column width-7" align="center">
                            <div class="hero-content-inner project-slide2">
                                <h2 class="mb-30"><g:message code="plan.slide1.topic"/></h2>
                                <hr/>
                                <table class="list-room">
                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_27.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.27"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 201</td>
                                    </tr>

                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_35-202.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.35"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 202 302 402 502 602 702 802</td>
                                    </tr>

                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_35-203.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.35"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 203 303 403 503 603 703 803</td>
                                    </tr>

                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_35-211.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.35"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 211 311 411 511 611 711 811</td>
                                    </tr>

                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_35-212.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.35"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 212 312 412 512 612 712 812</td>
                                    </tr>

                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_35-213.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.35"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 213 313 413 513 613 713 813</td>
                                    </tr>

                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_35-215.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.35"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 215 315 415 515 615 715 815</td>
                                    </tr>

                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_35-217.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.35"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 217 317 417 517 617 717 817</td>
                                    </tr>

                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_35-216.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.35"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 216 316 416 516 616 716 816</td>
                                    </tr>

                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_35-218.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.35"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 218 318 418 518 618 718 818</td>
                                    </tr>

                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_35-808.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.35"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 808</td>
                                    </tr>

                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_40.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.40"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 204 304 504 604 704 804</td>
                                    </tr>

                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_43-308.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.43"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 308 408 508 608 708</td>
                                    </tr>

                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_43-807.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.43"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 807</td>
                                    </tr>

                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_48.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.48"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 306 406 506 606 706</td>
                                    </tr>

                                    <tr onclick="changPlan('/assets/front/plan_img/one/one_49.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.oneBed.49"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 307 407 507 607 707</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="column width-5 ">
                            <a class="lightbox-link viewOne" data-toolbar="zoom"
                               data-group="gallery-1" href="/assets/front/plan_img/one/one_27.jpg">
                                <img src="/assets/front/plan_img/one/one_27.jpg" alt="" class="img-width imageOne">
                            </a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="row no-margins">
                        <div class="column width-7" align="center">
                            <div class="hero-content-inner project-slide2">
                                <h2 class="mb-30"><g:message code="plan.slide2.topic"/></h2>
                                <hr/>
                                <table class="list-room">
                                    <tr onclick="changPlan2('/assets/front/plan_img/two/two_54.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.twoBed.54"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 214 314 414 514 614 714 814</td>
                                    </tr>

                                    <tr onclick="changPlan2('/assets/front/plan_img/two/two_56.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.twoBed.56"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 301 401 501 601 701 801</td>
                                    </tr>

                                    <tr onclick="changPlan2('/assets/front/plan_img/two/two_63.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.twoBed.63"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 805</td>
                                    </tr>

                                    <tr onclick="changPlan2('/assets/front/plan_img/two/two_65.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.twoBed.65"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 809</td>
                                    </tr>

                                    <tr onclick="changPlan2('/assets/front/plan_img/two/two_72.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.twoBed.72"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 205 305 405 505 605 705</td>
                                    </tr>

                                    <tr onclick="changPlan2('/assets/front/plan_img/two/two_75.jpg')" class="list-room-tr">
                                        <td class="room-topic"><g:message code="plan.twoBed.75"/></td>
                                        <td class="room-left"><span class="room-number"><g:message code="plan.room"/></span> 209 309 409 509 609 709</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="column width-5 ">
                            %{--<img class="imgplan img-width" src="/assets/front/plan_img/two/two_54.jpg" alt="">--}%
                            <a class="lightbox-link viewTwo" data-toolbar="zoom"
                               data-group="gallery-2" href="/assets/front/plan_img/two/two_54.jpg">
                                <img src="/assets/front/plan_img/two/two_54.jpg" alt="" class="img-width imageTwo">
                            </a>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
        <!--slide End-->

        </div>
        <!-- Content End -->

        <g:render template="footer"/>

    </div>
</div>

<!-- Js -->
%{--<asset:javascript src="front/jquery-3.1.1.min.js"/>--}%
<asset:javascript src="front/jquery-1.12.4.min.js"/>
<asset:javascript src="front/timber.master.min.js"/>
<asset:javascript src="jquery.bxslider/jquery.bxslider.min.js"/>
<script type="text/javascript">
    function changPlan(src) {
        $('.imageOne').fadeOut(400, function () {
            $('.viewOne').attr("href",src)
            $('.imageOne').attr("src", src)
        }).fadeIn(400);
    }

    function changPlan2(src) {
        $('.imageTwo').fadeOut(400, function () {
            $('.viewTwo').attr("href",src)
            $('.imageTwo').attr("src", src)
        }).fadeIn(400);
    }

    $(document).ready(function () {

        var slider = $('.bxslider').bxSlider({
            controls: false
        });

        $('#slide-to2').on('click', function () {

            slider.goToSlide(1);

        });
        $('#slide-to1').on('click', function () {

            slider.goToSlide(0);

        });
        $("tr").click(function(){
            $(".list-room-tr").removeClass("text-active")
            $(this).addClass("text-active");
        });
    });

</script>

</body>
</html>