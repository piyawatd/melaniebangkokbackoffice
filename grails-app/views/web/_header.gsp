<ul>
    <li><a href="<g:createLink action="index"/>"><g:message code="header.navbar.home"/></a></li>
    <li><a href="<g:createLink action="register"/>"><g:message code="header.navbar.register"/></a></li>
    <li><a href="<g:createLink action="project"/>"><g:message code="header.navbar.project"/></a></li>
    <li><a href="<g:createLink action="plan"/>"><g:message code="header.navbar.unitPlan"/></a></li>
    <li><a href="<g:createLink action="gallery"/>"><g:message code="header.navbar.gallery"/></a></li>
    <li><a href="<g:createLink action="news"/>"><g:message code="header.navbar.newsProgress"/></a></li>
    <li><a href="<g:createLink action="contact"/>"><g:message code="header.navbar.contact"/></a></li>
    <li><a href="<g:createLink action="developer"/>"><g:message code="header.navbar.developer"/></a></li>
</ul>
