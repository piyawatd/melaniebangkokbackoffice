<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
    <meta name="google-site-verification" content="gM5mWvNHSbp8jqd3dM7H3WF4ntINT5pUzaZljq3p8EY" />
    <meta name="twitter:widgets:theme" content="light">
    <meta property="og:title" content="Melanie Bangkok ถนนจันทร์-สาทร"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="http://www.melaniebangkok.com/images/slide3.jpg"/>
    <meta property="og:description" content="Melanie Bangkok ถนนจันทร์-สาทร"/>
    <title>Melanie Bangkok ถนนจันทร์-สาทร</title>
    <link rel="shortcut icon" type="image/x-icon" href="/assets/front/theme-mountain-favicon.ico">

    <!-- Css -->
    <g:if test="${session.lang == "Th"}">
        <asset:stylesheet src="front/thai-fonts.css"/>
    </g:if>
    <g:if test="${session.lang == "En"} ${session.lang == "Ch"}">
        <asset:stylesheet src="front/eng-fonts.css"/>
    </g:if>
    <asset:stylesheet src="front/core.min.css"/>
    <asset:stylesheet src="front/skin-architecture.css"/>
    <asset:stylesheet src="front/header.css"/>
    <asset:stylesheet src="front/index.css"/>
    <asset:stylesheet src="front/footer.css"/>


</head>
<body class="shop home-page ">

<g:render template="headerCollapse"/>

<div class="wrapper reveal-side-navigation">
    <div class="wrapper-inner">

        <!-- Header -->
        <header class="header header-fixed header-bottom header-fixed-on-mobile header-transparent"
                data-sticky-threshold="window-height" data-bkg-threshold="100">
            <div class="header-inner">
                <div class="row nav-bar clear-row-mg">
                    <div class="column width-12 nav-bar-inner">
                        <div class="logo">
                            <div class="logo-inner">
                                <a href="<g:createLink action="index"/>"><img src="/assets/front/logo.png" alt="melanie Logo"/></a>
                                <a href="<g:createLink action="index"/>"><img src="/assets/front/logo.png" alt="melanie Logo"/></a>
                            </div>
                        </div>
                        <div>
                            <nav class="navigation nav-block secondary-navigation nav-right">
                                <ul>
                                    <li class="aux-navigation hide">
                                        <!-- Aux Navigation -->
                                        <a href="#" class="navigation-show side-nav-show nav-icon">
                                            <span class="icon-menu"></span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="pull-right language-key">
                            <a href="<g:createLink action="index" params="[lang:'Th']"/>"><strong>TH</strong></a> |
                            <a href="<g:createLink action="index" params="[lang:'En']"/>"><strong>EN</strong></a>
                            | <a href="<g:createLink action="index" params="[lang:'zh_Cn']"/>"><strong>中文</strong></a>
                        </div>
                        <nav class="navigation nav-block primary-navigation nav-right no-margin-right link-hilight">
                            <g:render template="header"/>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header End -->

        <div class="section-block register-tab hide show-on-mobile2">
            <div class="row">
                <a href="<g:createLink action="register"/>"><strong><g:message code="index.register.collapse"/></strong></a>
            </div>
        </div>
        %{--Form register mobile--}%
        <div class="register-popup">
            <div class="form-logo-top">
                <img src="/assets/front/form_head.png">
            </div>
            <div class="column width-12 bg-regist-body">
                <div class="contact-form-container">
                    <form class="contact-form" action="recordRegister" method="POST" id="registerForm">
                        <div class="row clear-row-register">
                            <div class="column width-12">
                                <div class="field-wrapper">
                                    <input type="text" name="firstName" id="firstName" class="form-element small form-box" placeholder="<g:message code="register.label.firstName"/>" required>
                                </div>
                            </div>
                            <div class="column width-12">
                                <div class="field-wrapper">
                                    <input type="text" name="lastName" id="lastName" class="form-element small form-box" placeholder="<g:message code="register.label.lastName"/>" required>
                                </div>
                            </div>
                            <div class="column width-12">
                                <div class="field-wrapper">
                                    <input type="text" name="tel" id="tel" class="form-element small form-box" placeholder="<g:message code="register.label.phone"/>" required>
                                </div>
                            </div>
                            <div class="column width-12">
                                <div class="field-wrapper">
                                    <input type="email" name="email" id="email" class="form-element small form-box" placeholder="<g:message code="register.label.email"/>" required>
                                </div>
                            </div>

                            <div class="column width-12">
                                <div class="form-select form-element small form-box">
                                    <select name="unitType" class="form-aux">
                                        <option selected="selected" value="1"><g:message code="register.label.oneBed"/></option>
                                        <option value="2"><g:message code="register.label.twoBed"/></option>
                                    </select>
                                </div>
                            </div>
                            <div class="column width-12">
                                <div class="form-select form-element small form-box">
                                    <select name="unitSize" class="form-aux">
                                        <option selected="selected" value="1"><g:message code="register.label.size1"/></option>
                                        <option value="2">35-40</option>
                                        <option value="3">41-50</option>
                                        <option value="4">51-60</option>
                                        <option value="5">61-70</option>
                                        <option value="6">71-80</option>
                                        <option value="7">81-90</option>
                                        <option value="8">91-100</option>
                                        <option value="9"><g:message code="register.label.size2"/></option>
                                    </select>
                                </div>
                            </div>
                            <div class="column width-12">
                                <div class="form-select form-element small form-box">
                                    <select name="priceRange" class="form-aux">
                                        <option selected="selected" value="1"><g:message code="register.label.range1"/></option>
                                        <option value="2"><g:message code="register.label.range2"/></option>
                                        <option value="3"><g:message code="register.label.range3"/></option>
                                        <option value="4"><g:message code="register.label.range4"/></option>
                                        <option value="5"><g:message code="register.label.range5"/></option>
                                        <option value="6"><g:message code="register.label.range6"/></option>
                                        <option value="7"><g:message code="register.label.range7"/></option>
                                        <option value="8"><g:message code="register.label.range8"/></option>
                                    </select>
                                </div>
                            </div>
                            <div class="column width-12">
                                <div class="form-select form-element small form-box">
                                    <select name="goal" class="form-aux">
                                        <option selected="selected" value="1">จุดประสงค์ในการซื้อ</option>
                                        <option value="2">บ้านหลังแรก</option>
                                        <option value="3">บ้านหลังที่สอง</option>
                                        <option value="4">เพื่อลงทุน (ขายต่อ)</option>
                                        <option value="5">เพื่อลงทุน (ปล่อยเช่า)</option>
                                        <option value="6">เพื่ออยู่อาศัยเอง</option>
                                        <option value="7">ใกล้ที่ทำงาน</option>
                                    </select>
                                </div>
                            </div>
                            <div class="column width-12">
                                <div class="field-wrapper checkbox-align">
                                    <input id="showCase" class="checkbox" name="showCase" type="checkbox">
                                    <label for="showCase" class="checkbox-label">รับชมตัวอย่างห้องโครงการ</label>
                                </div>
                            </div>
                            <div class="column width-12" align="center">
                                <input type="button" onclick="javascript:sendLine();" value="ลงทะเบียนรับสิทธิพิเศษ" style="width: auto;"
                                       class="button medium regist-submit">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="form-logo-bottom">
                <img src="/assets/front/form_bottom.png">
            </div>
        </div>
        %{--Form register mobile End--}%
        <!-- Content -->
        <div class="content clearfix">
            <section class="section-block featured-media window-height tm-slider-parallax-container">
                <div class="tm-slider-container full-width-slider pagination-top" data-featured-slider
                     data-parallax data-parallax-fade-out data-animation="fade" data-auto-advance
                     data-auto-advance-interval="4000" data-nav-pagination="false" data-scale-under="960"
                     data-progress-bar="false">
                    <ul class="tms-slides">
                        <g:each in="${bannerList}">
                        <li class="tms-slide" data-image data-force-fit>
                            <div class="tms-content">
                                <div class="tms-content bgcab hide-on-mobile hide-register">
                                    <div class="vdo-box hide-on-mobile">
                                        <a class="lightbox-link" data-toolbar="zoom" data-group="gallery-1" data-caption="Melanie Bangkok"
                                           href="http://www.youtube.com/embed/bMhAp2ZSvzM">
                                            <button class="button medium bkg-grey-light btn-vdo color-hover-white"><g:message code="index.vdoPreview"/></button>
                                        </a>
                                    </div>
                                    <div class="vdo-group hide show-on-mobile show-mobile">
                                        <div class="row">
                                            <div class="column width-12 vdo-caption">
                                                <a class="lightbox-link" data-toolbar="zoom" data-group="gallery-1" data-caption="Melanie Bangkok"
                                                   href="http://www.youtube.com/embed/bMhAp2ZSvzM">
                                                    <button class="button medium bkg-grey-light btn-vdo color-hover-white"><g:message code="index.vdoPreview"/></button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <img data-src="<g:resource dir="uploadresource/banner/" file="${it["image${lang}"]}"/>" data-retina src="/assets/front/blank.png" alt=""/>
                        </li>
                        </g:each>
                    </ul>
                </div>
            </section>



            <!-- Footer1 -->
            <footer class="section-block footer-center">
                <div class="row">
                    <ul>
                        <li class="mg-right">CALL 092 925 2555</li>
                        <li><a target="_blank" href="mailto:info@primegatesrealty.com"><i class="icon-mail"
                                                                                          aria-hidden="true"></i></a></li>
                        <li><a target="_blank" href="https://www.facebook.com/melaniebangkok/"><i class="icon-facebook"
                                                                                                  aria-hidden="true"></i>
                        </a>
                        </li>
                        <li><a target="_blank" href="https://www.instagram.com/melanie_bangkok/"><i class="icon-instagram"
                                                                                                    aria-hidden="true"></i>
                        </a>
                        </li>
                        <li><img src="/assets/front/images/footer_05.jpg"></li>
                        <li><img src="/assets/front/images/footer_06.jpg"></li>
                    </ul>
                </div>
            </footer>
            <!-- Footer1 End -->

            <div class="section-block bkg-charcoal bg-summary">
                <div class="row">
                    <div class="column width-12 news-topic">
                        <h2><g:message code="executive.topic"/></h2>
                        <img src="/assets/front/line.gif">
                    </div>
                    <div class="column width-12 executive-content">
                        <p><g:message code="executive.content"/></p>
                    </div>
                    <div class="no-bg-viewmore">
                        <a href="<g:createLink action="executive"/>" class="button medium bkg-grey-light btn-viewmore color-hover-white"><g:message code="index.viewmore"/></a>
                    </div>
                </div>
            </div>

            <!-- New progress -->
            <div id="news" class="section-block team-2 bkg-charcoal">
                <div class="row horizon" data-animate-in="preset:slideInRightShort;duration:1000ms;"
                     data-threshold="0.2">
                    <div class="column width-12 news-topic">
                        <h2><g:message code="index.newsProgress"/></h2>
                        <img src="/assets/front/line.gif">
                    </div>
                    <div class="column width-12">
                        <div class="row content-grid-3">
                            <g:each in="${newsList}">
                                <div class="grid-item">
                                    <div class="team-content">
                                        <div class="thumbnail no-margin-bottom img-scale-in" data-hover-easing="easeInOut"
                                             data-hover-speed="500" data-hover-bkg-color="#ffffff"
                                             data-hover-bkg-opacity="0.9">
                                            <a class="overlay-link fade-location" href="<g:createLink action="newsDetail" id="${it.getId()}"/>">
                                                <img src="<g:resource dir="uploadresource/news/" file="${it["image${lang}"]}"/>" width="760" height="500"/>
                                            </a>
                                        </div>
                                        <div class="team-content-info">
                                            <h4 class="news-title" style="line-height: 1.5">${it["title${lang}"]}</h4>
                                            <!--<h6 class="occupation mb-20">Founder / Architect</h6>-->
                                            <p>${it["description${lang}"]}</p>
                                        </div>
                                    </div>
                                </div>
                            </g:each>
                        </div>
                    </div>
                </div>
                <div class="no-bg-viewmore">
                    <a href="<g:createLink action="news"/>" class="button medium bkg-grey-light btn-viewmore color-hover-white"><g:message code="index.viewmore"/></a>
                </div>
            </div>
            <!-- New progress End -->




            <!-- Portfolio Grid -->
            <div class="section-block grid-container fade-in-progressively full-width no-margins bg-gallery"
                 data-layout-mode="masonry" data-grid-ratio="1.5" data-animate-filter-duration="700" data-set-dimensions
                 data-animate-resize data-animate-resize-duration="0">
                <div class="row">
                    <div class="column width-12">
                        <div class="row grid content-grid-3">
                            <g:each in="${galleryLists}">
                                <div class="grid-item grid-sizer">
                                    <div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="500" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="1">
                                        <a class="overlay-link" data-toolbar="zoom" data-group="gallery-1" data-caption="Melanie Bangkok - Chan-Sathon" href="<g:createLink action="galleryList" id="${it.getId()}"/>">
                                            <img src="<g:resource dir="uploadresource/gallery/" file="${it["imageName"]}"/>" alt=""/>
                                            <span class="overlay-info">
                                                <span>
                                                    <span>
                                                        <span class="project-title">${it["title${lang}"]}</span>
                                                        <span class="project-description">${it["detail${lang}"]}</span>
                                                    </span>
                                                </span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </g:each>
                        </div>
                    </div>
                </div>
                <div class="bg-viewmore pad-gallery">
                    <a href="<g:createLink action="gallery"/>" class="button medium bkg-grey-light btn-viewmore color-hover-white"><g:message code="index.viewmore"/></a>
                </div>
            </div>
            %{--<!--Portfolio Grid End -->--}%





        </div>
        <!-- Content End -->

        <!-- Footer2 -->
        <footer class="section-block footer-center">
            <div class="row">
                <ul class="pt-10 pb-10">
                    <li class="mg-right">&copy; 2016 Melanie Bangkok.</li>
                </ul>
            </div>
        </footer>
        <!-- Footer2 End -->

    </div>
</div>

<!-- Js -->
<asset:javascript src="front/jquery-3.1.1.min.js"/>
<asset:javascript src="front/timber.master.min.js"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108532517-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-108532517-1');
    
    function sendLine() {
        //priceRange
        var priceRange = $('select[name=priceRange]').val();
        var priceRangeValue = "";
        switch (priceRange) {
            case "1" :
                priceRangeValue = "Below 4 Mil.Baht";
                break;
            case "2" :
                priceRangeValue = "4-5 Mil.Baht";
                break;
            case "3" :
                priceRangeValue = "5.1-6 Mil.Baht";
                break;
            case "4" :
                priceRangeValue = "6.1-7 Mil.Baht";
                break;
            case "5" :
                priceRangeValue = "7.1-8 Mil.Baht";
                break;
            case "6" :
                priceRangeValue = "8.1-9 Mil.Baht";
                break;
            case "7" :
                priceRangeValue = "9.1-10 Mil.Baht";
                break;
            case "8" :
                priceRangeValue = "10 Mil. Baht";
                break;
        }
        //priceRange End


        //unitType
        var unitType = $('select[name=unitType]').val();
        var unitTypeValue = "";
        switch (unitType){
            case "1":
                unitTypeValue = "1 Bed";
                break;
            case "2":
                unitTypeValue = "2 Bed";
                break;
        }
        //unitType End


        //unitSize
        var unitSize = $('select[name=unitSize]').val();
        var unitSizeValue = "";
        switch (unitSize) {
            case "1" :
                unitSizeValue = "Below 35";
                break;
            case "2" :
                unitSizeValue = "35-40";
                break;
            case "3" :
                unitSizeValue = "41-50";
                break;
            case "4" :
                unitSizeValue = "51-60";
                break;
            case "5" :
                unitSizeValue = "61-70";
                break;
            case "6" :
                unitSizeValue = "71-80";
                break;
            case "7" :
                unitSizeValue = "81-90";
                break;
            case "8" :
                unitSizeValue = "91-100";
                break;
            case "9" :
                unitSizeValue = "Above 100";
                break;
        }
        //unitSize End

        //goal
        var goal = $('select[name=goal]').val();
        var goalValue = "";
        switch (goal) {
            case "1" :
                goalValue = "จุดประสงค์ในการซื้อ";
                break;
            case "2" :
                goalValue = "บ้านหลังแรก";
                break;
            case "3" :
                goalValue = "บ้านหลังที่สอง";
                break;
            case "4" :
                goalValue = "เพื่อลงทุน (ขายต่อ)";
                break;
            case "5" :
                goalValue = "เพื่อลงทุน (ปล่อยเช่า)";
                break;
            case "6" :
                goalValue = "เพื่ออยู่อาศัยเอง";
                break;
            case "7" :
                goalValue = "ใกล้ที่ทำงาน";
                break;
        }
        //goal End

        //showCase
        var showCaseValue = "";
        if($('#showCase').is(":checked"))
        {
            showCaseValue = "รับชมตัวอย่างห้องโครงการ";
        }else{
            showCaseValue = "ไม่รับชมตัวอย่างห้องโครงการ";
        }
        //showCase End
        //           Line notify
        var messageText = "\n" + ($('#firstName').val()) + " " + $('#lastName').val() + "\n" + $('#tel').val() + "\n" + $('#email').val() + "\n" + priceRangeValue + "\n" + unitTypeValue + "\n" + unitSizeValue + "\n" + goalValue + "\n" + showCaseValue;
        $.ajax
        ({
            type: "POST",
            contentType:"application/x-www-form-urlencoded",
                url: "http://huskiesgifts.com:9000/line/notify.php",
//            url: "http://192.168.1.14:8888/melaniebangkok/line/notify.php",
            dataType:"jsonp",
            data: {message:messageText},
            success: function (){
                %{--alert('<g:message code="lineNotify.message.reg"/>');--}%
//                $("#registerForm").submit();
                document.getElementById("registerForm").submit();
            }
        });
        //           Line notify End
    }

</script>

</body>
</html>