<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
    <meta name="format-detection" content="telephone=no"/>
    <meta name="twitter:widgets:theme" content="light">
    <meta property="og:title" content="Melanie Bangkok ถนนจันทร์-สาทร"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="http://www.melaniebangkok.com/images/slide3.jpg"/>
    <meta property="og:description" content="Melanie Bangkok ถนนจันทร์-สาทร"/>
    <title>Melanie Bangkok ถนนจันทร์-สาทร</title>
    <link rel="shortcut icon" type="image/x-icon" href="/assets/front/theme-mountain-favicon.ico">



    <!-- Css -->
    <g:if test="${session.lang == "Th"}">
        <asset:stylesheet src="front/thai-fonts.css"/>
    </g:if>
    <g:if test="${session.lang == "En"} ${session.lang == "Ch"}">
        <asset:stylesheet src="front/eng-fonts.css"/>
    </g:if>
    <asset:stylesheet src="front/core.min.css"/>
    <asset:stylesheet src="front/skin-architecture.css"/>
    <asset:stylesheet src="front/font-awesome.min.css.css"/>
    <asset:stylesheet src="front/header.css"/>
    <asset:stylesheet src="front/contact.css"/>
    <asset:stylesheet src="front/footer.css"/>

</head>

<body class="shop home-page">

<g:render template="headerCollapse"/>

<div class="wrapper reveal-side-navigation">
    <div class="wrapper-inner" style="filter: none">

        <!-- Header -->
        <header class="header header-fixed header-top header-fixed-on-mobile header-transparent" data-sticky-threshold="window-height" data-bkg-threshold="100">
            <div class="header-inner">
                <div class="row nav-bar clear-row-mg">
                    <div class="column width-12 nav-bar-inner">
                        <div class="logo">
                            <div class="logo-inner">
                                <a href="<g:createLink action="index"/>"><img src="/assets/front/logo.png" alt="melanie Logo"/></a>
                                <a href="<g:createLink action="index"/>"><img src="/assets/front/logo.png" alt="melanie Logo"/></a>
                            </div>
                        </div>
                        <div>
                            <nav class="navigation nav-block secondary-navigation nav-right">
                                <ul>
                                    <li class="aux-navigation hide">
                                        <!-- Aux Navigation -->
                                        <a href="#" class="navigation-show side-nav-show nav-icon">
                                            <span class="icon-menu"></span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="pull-right language-key">
                            <a href="<g:createLink action="contact" params="[lang:'Th']"/>"><strong>TH</strong></a> |
                            <a href="<g:createLink action="contact" params="[lang:'En']"/>"><strong>EN</strong></a>
                            | <a href="<g:createLink action="contact" params="[lang:'zh_Cn']"/>"><strong>中文</strong></a>
                        </div>
                        <nav class="navigation nav-block primary-navigation nav-right no-margin-right link-hilight">
                            <g:render template="header"/>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header End -->


        <!-- Content -->
        <div class="content clearfix">

            <!-- Sign Up Section 2 -->
            <!-- About Section -->
            <div id="contact" class="section-block  pb-40 pt-10 form-deco">
                <img src="/assets/front/line.png" class="mb-20 img-line"/>

                <div class="row">
                    <div class="column width-5 content-contact">
                        <h3 class="mb-30"><g:message code="contact.topic"/></h3>

                        <p><g:message code="contact.address"/></p>

                        <p><i class="fa fa-phone" aria-hidden="true"></i>
                            <g:message code="contact.tel"/>
                            <br/>
                            <i class="icon-mail" aria-hidden="true"></i>
                            info@primegatesrealty.com</p>

                        <h3 class="mb-30"><g:message code="contact.topic2"/></h3>

                        <p><g:message code="contact.subtopic2"/><br><g:message code="contact.address2"/></p>

                        <p><i class="icon-mail" aria-hidden="true"></i>
                            info@primegatesrealty.com</p>
                    </div>

                    <div class="column width-5 offset-2">
                        <div class="hero-content-inner center content-contact2 column clear-pd">
                            <h3 class="mb-10"><g:message code="contact.formTopic"/></h3>
                            <img src="/assets/front/line.gif" class="mb-10"/>

                            <p><g:message code="contact.formDetail2"/></p>
                        </div>

                        <form action="recordContact" method="post" id="contactForm">
                            <div class="row">
                                <div class="column width-12">
                                    <input type="text" name="name" id="name" class="form-name form-element small"
                                           placeholder="<g:message code="contact.label.name"/>" tabindex="1" required>
                                </div>

                                <div class="column width-12">
                                    <input type="email" name="email" id="email" class="form-email form-element small"
                                           placeholder="<g:message code="contact.label.email"/>" tabindex="3" required>
                                </div>

                                <div class="column width-12">
                                    <input type="text" name="phone" id="phone" class="form-phone form-element small"
                                           placeholder="<g:message code="contact.label.phone"/>" tabindex="3" required>
                                </div>

                                <div class="column width-12">
                                    <div class="field-wrapper">
                                        <textarea name="message" id="message" class="form-message form-element small"
                                                  placeholder="<g:message code="contact.label.message"/>" tabindex="4" required></textarea>
                                    </div>
                                </div>

                                <div class="column width=12 recapcha">
                                    <div id="recaptcha"></div>
                                </div>

                                <div class="column width-12 submit-align">
                                    <input type="button" value="<g:message code="contact.submit"/>" style="width: auto"
                                           class="form-submit button button-text button-text-hover" onclick="javascript:submitForm()">
                                </div>

                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <!-- About Section End -->

            <!-- Hero 5 Section -->
            <div class="show-media-column-on-mobile bg-map">
                <div>
                    <div class="img-map">
                        <div class="column width-6 no-padding map-local">
                            <img src="/assets/front/map.jpg">
                        </div>
                    </div>

                    <div class="gg-map">
                        <div class="column width-6 no-padding">
                            <div class="map-container" data-coordinates="[[13.7055944,100.5349268]]"
                                 data-icon='"/assets/front/assets/map-marker-2.png"' data-info='"Melanie Bangkok"'
                                 data-zoom-level="16">
                                <div id="map-canvas"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Hero 5 Section End -->

            <div class="section-block">
                <g:if test="${contact}">
                    ${raw(contact["detail${lang}"])}
                </g:if>
            </div>



    </div>
    <!-- Content End -->

    <g:render template="footer"/>

</div>

<!-- Js -->
<asset:javascript src="front/jquery-3.1.1.min.js"/>
<asset:javascript src="front/timber.master.min.js"/>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAx9VbFdKjKdcEE8eDh-1wJTbqshb5zAS4"></script>
<script>
    $('body').append('<div class="product-image-overlay"><span class="product-image-overlay-close">x</span><img src="" /></div>');
    var productImage = $('img');
    var productOverlay = $('.product-image-overlay');
    var productOverlayImage = $('.product-image-overlay img');

    productImage.click(function () {
        var productImageSource = $(this).attr('src');

        productOverlayImage.attr('src', productImageSource);
        productOverlay.fadeIn(100);
        $('body').css('overflow', 'hidden');

        $('.product-image-overlay-close').click(function () {
            productOverlay.fadeOut(100);
            $('body').css('overflow', 'auto');
        });
    });
</script>

<script type="text/javascript">
    var verifyCallback = function(response) {
        alert(response);
    };
    var widgetId1;
    var onloadCallback = function() {
        // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
        // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
        widgetId1 = grecaptcha.render('recaptcha', {
            'sitekey' : '6Lf1ki4UAAAAANZiRzsPs7okZrLOqg92coNbq90A',
            'theme' : 'light'
        });
    };
    function submitForm()
    {
        if(grecaptcha.getResponse(widgetId1) != "")
        {
            //            Line notify
            var messageText = "\n" + ($('#name').val()) + "\n" + $('#phone').val() + "\n" + $('#email').val() + "\n" + $('#message').val()
            $.ajax
            ({
                type: "POST",
                contentType:"application/x-www-form-urlencoded",
                url: "http://huskiesgifts.com:9000/line/notifycontact.php",
//                url: "http://localhost:8888/melaniebangkok/app/notify.php",
                dataType:"jsonp",
                data: {message:messageText},
                success: function (){
                    alert('<g:message code="lineNotify.message.contact"/>');
                    $("#contactForm" ).submit();
                }
            });
//            Line notify End

        }
    }
</script>

<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>

</body>
</html>