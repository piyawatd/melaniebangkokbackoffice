<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
    <meta name="twitter:widgets:theme" content="light">
    <meta property="og:title" content="Melanie Bangkok ถนนจันทร์-สาทร"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="http://www.melaniebangkok.com/images/slide3.jpg"/>
    <meta property="og:description" content="Melanie Bangkok ถนนจันทร์-สาทร"/>
    <title>Melanie Bangkok ถนนจันทร์-สาทร</title>
    <link rel="shortcut icon" type="image/x-icon" href="/assets/front/theme-mountain-favicon.ico">

    <!-- Font -->


    <!-- Css -->
    <g:if test="${session.lang == "Th"}">
        <asset:stylesheet src="front/thai-fonts.css"/>
    </g:if>
    <g:if test="${session.lang == "En"} ${session.lang == "Ch"}">
        <asset:stylesheet src="front/eng-fonts.css"/>
    </g:if>
    <asset:stylesheet src="front/core.min.css"/>
    <asset:stylesheet src="front/skin-architecture.css"/>
    <asset:stylesheet src="front/header.css"/>
    <asset:stylesheet src="front/register.css"/>
    <asset:stylesheet src="front/footer.css"/>

</head>

<body class="shop home-page thai-text">

<g:render template="headerCollapse"/>

<div class="wrapper reveal-side-navigation">
    <div class="wrapper-inner">

        <!-- Header -->
        <header class="header header-fixed header-top header-fixed-on-mobile header-transparent" data-sticky-threshold="window-height" data-bkg-threshold="100">
            <div class="header-inner">
                <div class="row nav-bar clear-row-mg">
                    <div class="column width-12 nav-bar-inner">
                        <div class="logo">
                            <div class="logo-inner">
                                <a href="<g:createLink action="index"/>"><img src="/assets/front/logo.png" alt="melanie Logo"/></a>
                                <a href="<g:createLink action="index"/>"><img src="/assets/front/logo.png" alt="melanie Logo"/></a>
                            </div>
                        </div>
                        <div>
                            <nav class="navigation nav-block secondary-navigation nav-right">
                                <ul>
                                    <li class="aux-navigation hide">
                                        <!-- Aux Navigation -->
                                        <a href="#" class="navigation-show side-nav-show nav-icon">
                                            <span class="icon-menu"></span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="pull-right language-key">
                            <a href="<g:createLink action="register" params="[lang:'Th']"/>"><strong>TH</strong></a> |
                            <a href="<g:createLink action="register" params="[lang:'En']"/>"><strong>EN</strong></a>
                            | <a href="<g:createLink action="register" params="[lang:'zh_Cn']"/>"><strong>中文</strong></a>
                        </div>
                        <nav class="navigation nav-block primary-navigation nav-right no-margin-right link-hilight">
                            <g:render template="header"/>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header End -->


        <!-- Content -->
        <div class="content clearfix">

            <!-- Form Advanced -->
            <div class="section-block replicable-content bkg-grey-ultralight mg-top">
                <div class="row">
                    <div class="column width-12">
                        <h2 class="mb-30"><g:message code="register.label.register"/></h2>
                        <hr>
                    </div>

                    <div class="column width-12">
                        <div class="contact-form-container">
                            <form action="recordRegister" method="POST" id="registerForm">
                                <div class="row">
                                    <div class="column width-12">
                                        <div class="field-wrapper">
                                            <input type="text" name="firstName" id="firstName" class="form-fname form-element medium"
                                                   placeholder="<g:message code="register.label.firstName"/>"
                                                   tabindex="1" required>
                                        </div>
                                    </div>

                                    <div class="column width-12">
                                        <div class="field-wrapper">
                                            <input type="text" name="lastName" id="lastName" class="form-lname form-element medium"
                                                   placeholder="<g:message code="register.label.lastName"/>"
                                                   tabindex="2">
                                        </div>
                                    </div>

                                    <div class="column width-12">
                                        <div class="field-wrapper">
                                            <input type="text" name="tel" id="tel" class="form-phone form-element medium"
                                                   placeholder="<g:message code="register.label.phone"/>" tabindex="3"
                                                   required>
                                        </div>
                                    </div>

                                    <div class="column width-12">
                                        <div class="field-wrapper">
                                            <input type="email" name="email" id="email" class="form-email form-element medium"
                                                   placeholder="<g:message code="register.label.email"/>" tabindex="4"
                                                   required>
                                        </div>
                                    </div>

                                    <!--Price Range-->
                                    <div class="column width-12 mt-20">
                                        <h4><g:message code="register.label.priceRange"/></h4>

                                        <div class="field-wrapper pt-10 pb-10">

                                            <input id="radio-1" class="form-element radio" name="priceRange" type="radio" value="1" checked>
                                            <label for="radio-1" class="radio-label"><g:message
                                                    code="register.label.range1"/></label>

                                            <input id="radio-2" class="form-element radio" name="priceRange"
                                                   type="radio" value="2">
                                            <label for="radio-2" class="radio-label"><g:message code="register.label.range2"/></label>

                                            <input id="radio-3" class="form-element radio" name="priceRange"
                                                   type="radio" value="3">
                                            <label for="radio-3" class="radio-label"><g:message code="register.label.range3"/></label>

                                            <input id="radio-4" class="form-element radio" name="priceRange"
                                                   type="radio" value="4">
                                            <label for="radio-4" class="radio-label"><g:message code="register.label.range4"/></label>

                                            <input id="radio-5" class="form-element radio" name="priceRange"
                                                   type="radio" value="5">
                                            <label for="radio-5" class="radio-label"><g:message code="register.label.range5"/></label>

                                            <input id="radio-6" class="form-element radio" name="priceRange"
                                                   type="radio" value="6">
                                            <label for="radio-6" class="radio-label"><g:message code="register.label.range6"/></label>

                                            <input id="radio-7" class="form-element radio" name="priceRange"
                                                   type="radio" value="7">
                                            <label for="radio-7" class="radio-label"><g:message code="register.label.range7"/></label>

                                            <input id="radio-8" class="form-element radio" name="priceRange"
                                                   type="radio" value="8">
                                            <label for="radio-8" class="radio-label"><g:message code="register.label.range8"/></label>
                                        </div>
                                    </div>
                                    <!--Price Range End-->

                                    <!--Unit Type-->
                                    <div class="column width-12">
                                        <h4><g:message code="register.label.unitType"/></h4>

                                        <div class="field-wrapper pt-10 pb-10">
                                            <input id="radio-9" class="form-element radio" name="unitType"
                                                   type="radio" value="1" checked>
                                            <label for="radio-9" class="radio-label"><g:message
                                                    code="register.label.oneBed"/></label>

                                            <input id="radio-10" class="form-element radio" name="unitType"
                                                   type="radio" value="2">
                                            <label for="radio-10" class="radio-label"><g:message
                                                    code="register.label.twoBed"/></label>
                                        </div>
                                    </div>
                                    <!--Unit Type End-->

                                    <!--Unit Size-->
                                    <div class="column width-12">
                                        <h4><g:message code="register.label.unitSize"/></h4>

                                        <div class="field-wrapper pt-10 pb-10">
                                            <input id="radio-11" class="form-element radio" name="unitSize"
                                                   type="radio" value="1" checked>
                                            <label for="radio-11" class="radio-label"><g:message
                                                    code="register.label.size1"/></label>

                                            <input id="radio-12" class="form-element radio" name="unitSize"
                                                   type="radio" value="2">
                                            <label for="radio-12" class="radio-label">35-40</label>

                                            <input id="radio-13" class="form-element radio" name="unitSize"
                                                   type="radio" value="3">
                                            <label for="radio-13" class="radio-label">41-50</label>

                                            <input id="radio-14" class="form-element radio" name="unitSize"
                                                   type="radio" value="4">
                                            <label for="radio-14" class="radio-label">51-60</label>

                                            <input id="radio-15" class="form-element radio" name="unitSize"
                                                   type="radio" value="5">
                                            <label for="radio-15" class="radio-label">61-70</label>

                                            <input id="radio-16" class="form-element radio" name="unitSize"
                                                   type="radio" value="6">
                                            <label for="radio-16" class="radio-label">71-80</label>

                                            <input id="radio-17" class="form-element radio" name="unitSize"
                                                   type="radio" value="7">
                                            <label for="radio-17" class="radio-label">81-90</label>

                                            <input id="radio-18" class="form-element radio" name="unitSize"
                                                   type="radio" value="8">
                                            <label for="radio-18" class="radio-label">91-100</label>

                                            <input id="radio-19" class="form-element radio" name="unitSize"
                                                   type="radio" value="9">
                                            <label for="radio-19" class="radio-label"><g:message
                                                    code="register.label.size2"/></label>
                                        </div>
                                    </div>
                                    <!--Unit Size End-->

                                    <!--goal-->
                                    <div class="column width-12">
                                        <h4>จุดประสงค์ในการซื้อ</h4>

                                        <div class="field-wrapper pt-10 pb-10">
                                            <input id="radio-20" class="form-element radio" name="goal"
                                                   type="radio" value="1" checked>
                                            <label for="radio-20" class="radio-label">จุดประสงค์ในการซื้อ</label>

                                            <input id="radio-21" class="form-element radio" name="goal"
                                                   type="radio" value="2">
                                            <label for="radio-21" class="radio-label">บ้านหลังแรก</label>

                                            <input id="radio-22" class="form-element radio" name="goal"
                                                   type="radio" value="3">
                                            <label for="radio-22" class="radio-label">บ้านหลังที่สอง</label>

                                            <input id="radio-24" class="form-element radio" name="goal"
                                                   type="radio" value="4">
                                            <label for="radio-24" class="radio-label">เพื่อลงทุน (ขายต่อ)</label>

                                            <input id="radio-25" class="form-element radio" name="goal"
                                                   type="radio" value="5">
                                            <label for="radio-25" class="radio-label">เพื่อลงทุน (ปล่อยเช่า)</label>

                                            <input id="radio-26" class="form-element radio" name="goal"
                                                   type="radio" value="6">
                                            <label for="radio-26" class="radio-label">เพื่ออยู่อาศัยเอง</label>

                                            <input id="radio-27" class="form-element radio" name="goal"
                                                   type="radio" value="7">
                                            <label for="radio-27" class="radio-label">ใกล้ที่ทำงาน</label>
                                        </div>
                                    </div>
                                    <!--goal End-->

                                    <div class="column width-12">
                                        <div class="field-wrapper checkbox-align">
                                            <input id="showCase" class="checkbox" name="showCase" type="checkbox">
                                            <label for="showCase" class="checkbox-label">รับชมตัวอย่างห้องโครงการ</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="column width-12 mt-10 mb-10">
                                        <h6><g:message code="register.label.please"/></h6>
                                    </div>

                                    <div class="column width=12 gg-capcha">
                                        <div id="recaptcha"></div>
                                    </div>

                                    <div class="column width-12">
                                        <input type="button" value="<g:message
                                                code="register.submit"/>" style="width: auto"
                                               class="form-submit button button-text button-text-hover" onclick="javascript:submitForm()">
                                    </div>

                                </div>
                            </form>


                            <div class="form-response"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Form Advanced End -->

        </div>
        <!-- Content End -->

        <g:render template="footer"/>

    </div>
</div>

<!-- Js -->
<asset:javascript src="front/jquery-3.1.1.min.js"/>
<asset:javascript src="front/timber.master.min.js"/>


<script type="text/javascript">
    var verifyCallback = function(response) {
        alert(response);
    };
    var widgetId1;
    var onloadCallback = function() {
        // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
        // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
        widgetId1 = grecaptcha.render('recaptcha', {
            'sitekey' : '6Lf1ki4UAAAAANZiRzsPs7okZrLOqg92coNbq90A',
            'theme' : 'light'
        });
    };
    function submitForm()
    {
        if(grecaptcha.getResponse(widgetId1) != "")
        {
	        console.log('in');
            //priceRange
            var priceRange = $('input[name=priceRange]:checked').val();
            var priceRangeValue = "";
            switch (priceRange) {
                case "1" :
                    priceRangeValue = "Below 4 Mil.Baht";
                    break;
                case "2" :
                    priceRangeValue = "4-5 Mil.Baht";
                    break;
                case "3" :
                    priceRangeValue = "5.1-6 Mil.Baht";
                    break;
                case "4" :
                    priceRangeValue = "6.1-7 Mil.Baht";
                    break;
                case "5" :
                    priceRangeValue = "7.1-8 Mil.Baht";
                    break;
                case "6" :
                    priceRangeValue = "8.1-9 Mil.Baht";
                    break;
                case "7" :
                    priceRangeValue = "9.1-10 Mil.Baht";
                    break;
                case "8" :
                    priceRangeValue = "10 Mil. Baht";
                    break;
            }
            //priceRange End


            //unitType
            var unitType = $('input[name=unitType]:checked').val();
            var unitTypeValue = "";
            switch (unitType){
                case "1":
                    unitTypeValue = "1 Bed";
                    break;
                case "2":
                    unitTypeValue = "2 Bed";
                    break;
            }
            //unitType End


            //unitSize
            var unitSize = $('input[name=unitSize]:checked').val();
            var unitSizeValue = "";
            switch (unitSize) {
                case "1" :
                    unitSizeValue = "Below 35";
                    break;
                case "2" :
                    unitSizeValue = "35-40";
                    break;
                case "3" :
                    unitSizeValue = "41-50";
                    break;
                case "4" :
                    unitSizeValue = "51-60";
                    break;
                case "5" :
                    unitSizeValue = "61-70";
                    break;
                case "6" :
                    unitSizeValue = "71-80";
                    break;
                case "7" :
                    unitSizeValue = "81-90";
                    break;
                case "8" :
                    unitSizeValue = "91-100";
                    break;
                case "9" :
                    unitSizeValue = "Above 100";
                    break;
            }
            //unitSize End

            //goal
            var goal = $('input[name=goal]:checked').val();
            var goalValue = "";
            switch (goal) {
                case "1" :
                    goalValue = "จุดประสงค์ในการซื้อ";
                    break;
                case "2" :
                    goalValue = "บ้านหลังแรก";
                    break;
                case "3" :
                    goalValue = "บ้านหลังที่สอง";
                    break;
                case "4" :
                    goalValue = "เพื่อลงทุน (ขายต่อ)";
                    break;
                case "5" :
                    goalValue = "เพื่อลงทุน (ปล่อยเช่า)";
                    break;
                case "6" :
                    goalValue = "เพื่ออยู่อาศัยเอง";
                    break;
                case "7" :
                    goalValue = "ใกล้ที่ทำงาน";
                    break;
            }
            //goal End

            //showCase
            var showCaseValue = "";
            if($('#showCase').is(":checked"))
            {
                showCaseValue = "รับชมตัวอย่างห้องโครงการ";
            }else{
                showCaseValue = "ไม่รับชมตัวอย่างห้องโครงการ";
            }
            //showCase End



     //           Line notify
            var messageText = "\n" + ($('#firstName').val()) + " " + $('#lastName').val() + "\n" + $('#tel').val() + "\n" + $('#email').val() + "\n" + priceRangeValue + "\n" + unitTypeValue + "\n" + unitSizeValue + "\n" + goalValue + "\n" + showCaseValue;
            $.ajax
            ({
                type: "POST",
                contentType:"application/x-www-form-urlencoded",
                url: "http://huskiesgifts.com:9000/line/notify.php",
//                url: "http://192.168.1.14:8888/melaniebangkok/line/notify.php",
                dataType:"jsonp",
                data: {message:messageText},
                success: function (){
//                    alert('Thanks for your register!');
                    %{--alert('<g:message code="lineNotify.message.reg"/>');--}%
                    $("#registerForm" ).submit();
                }
            });
 //           Line notify End
        }
    }
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
</body>
</html>