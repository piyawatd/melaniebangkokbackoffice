<!-- Footer -->
<footer class="section-block footer-center">
    <div class="row">
        <ul>
            <li class="mg-right">CALL 092 925 2555</li>
            <li><a target="_blank" href="mailto:info@primegatesrealty.com"><i class="icon-mail"
                                                                              aria-hidden="true"></i></a></li>
            <li><a target="_blank" href="https://www.facebook.com/melaniebangkok/"><i class="icon-facebook"
                                                                                      aria-hidden="true"></i>
            </a>
            </li>
            <li><a target="_blank" href="https://www.instagram.com/melanie_bangkok/"><i class="icon-instagram"
                                                                                        aria-hidden="true"></i>
            </a>
            </li>
            <li><img src="/assets/front/images/footer_05.jpg"></li>
            <li><img src="/assets/front/images/footer_06.jpg"></li>
        </ul>
    </div>
    <div class="row">
        <ul>
            <li>&copy; 2016 Melanie Bangkok.</li>
        </ul>
    </div>
</footer>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108532517-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-108532517-1');
</script>

<!-- Footer End -->