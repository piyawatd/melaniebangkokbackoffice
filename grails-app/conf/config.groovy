ckeditor {
    config = "/js/ckconfig.js"
    upload {
        basedir = "/uploads/"
        overwrite = true
        image {
            browser = true
            upload = true
            allowed = ['jpg', 'gif', 'jpeg', 'png']
            denied = []
        }
    }
}