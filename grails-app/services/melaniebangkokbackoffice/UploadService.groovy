package melaniebangkokbackoffice

import grails.transaction.Transactional
import grails.util.Holders

import java.nio.file.Files

@Transactional
class UploadService {

    def getPathResource()
    {
        def grailsApplication = Holders.getGrailsApplication()
        return grailsApplication.config.pathUpload
    }

    def uploadFile(request,String fieldName,String pathFile,String prefixName,String typeFile) {
        // Create a File object representing the folder
        def grailsApplication = Holders.getGrailsApplication()
        String folderPath = "${grailsApplication.config.pathUpload}${pathFile}"
        def (folder,nFileName) = [new File(folderPath),""]
        // If it doesn't exist
        if( !folder.exists() ) {
            // Create all folders up-to and including B
            folder.mkdirs()
        }
        List fileList = request.getFiles(fieldName)
        def result = true
        if(fileList.size() > 0){
            def f = fileList.get(0)
            def oriFileName = f.getOriginalFilename().split("\\.")
            def oriFileType = oriFileName[(oriFileName.size()-1)]
            switch (typeFile){
                case "image":
                    def lst = ['png','jpg','jpeg']
                    if(!lst.contains(oriFileType.toLowerCase()))
                    {
                        result = false
                    }
                    break
            }
            if(result)
            {
                nFileName = "${prefixName}${oriFileName[0].replaceAll(" ","")}_${UUID.randomUUID().toString().replaceAll("-","")}.${oriFileName[(oriFileName.size()-1)]}"
                def fullPath = "${folderPath}/${nFileName}"
                f.transferTo(new File(fullPath))
            }
        }
        return nFileName
    }

    def moveFile(String fieldName,String filePath,String desPath){
        def grailsApplication = Holders.getGrailsApplication()
        def folder = new File("${grailsApplication.config.pathUpload}${desPath}")
        // If it doesn't exist
        if( !folder.exists() ) {
            // Create all folders up-to and including B
            folder.mkdirs()
        }
        new File("${grailsApplication.config.pathUpload}${desPath}/${fieldName}") << new File("${grailsApplication.config.pathUpload}${filePath}/${fieldName}").bytes
        boolean fileSuccessfullyDeleted = new File("${grailsApplication.config.pathUpload}${filePath}/${fieldName}").delete()
    }

    def deleteFile(String fieldName,String filePath){
        def grailsApplication = Holders.getGrailsApplication()
        boolean fileSuccessfullyDeleted = new File("${grailsApplication.config.pathUpload}${filePath}/${fieldName}").delete()
    }
}
