package melaniebangkokbackoffice

import grails.converters.JSON

class BannerController {

    def uploadService

    def index() {
        def bannerList = Banner.list(sort:"sequence",order:"asc")
        [bannerList:bannerList]
    }

    def show() {
        def banner = Banner.get(params.id)
        withFormat {
            html {}
            json {
                if(params.containsKey('callback'))
                {
                    render "${params.callback}(${banner as JSON})"
                }else{
                    render banner as JSON
                }
            }
        }
    }

    def newForm() {
        Banner banner = new Banner()
        [banner:banner]
    }

    def editForm() {
        Banner banner = Banner.get(params.id)
        [banner:banner]
    }

    def create() {
        Banner banner = new Banner(params)
        banner.setSequence((Banner.count()+1))
        banner.setActive(true)
        if (!banner.save())
        {
            flash.error = "Have Error"
            render(view: "newForm", model: [banner: banner])
        }else{
            if(params.containsKey('imageTh') && params["imageTh"] != "")
            {
                uploadService.moveFile(banner.getImageTh(),'temp','banner')
            }
            if(params.containsKey('imageEn') && params["imageEn"] != "")
            {
                uploadService.moveFile(banner.getImageEn(),'temp','banner')
            }
            if(params.containsKey('imageCh') && params["imageCh"] != "")
            {
                uploadService.moveFile(banner.getImageCh(),'temp','banner')
            }
            flash.message = "Create Complete."
            redirect(action: "index")
        }
    }

    def update() {
        Banner banner = Banner.get(params.id)
        String oldFileNameTh = banner.getImageTh()
        println oldFileNameTh
        String oldFileNameEn = banner.getImageEn()
        String oldFileNameCh = banner.getImageCh()
        banner.properties = params
        if(params.containsKey('imageTh') && params["imageTh"] == "delete")
        {
            banner.setImageTh("")
        }else{
            if(params.containsKey('imageTh') && params["imageTh"] != "")
            {
                banner.setImageTh(params["imageTh"])
            }else{
                banner.setImageTh(oldFileNameTh)
            }
        }
        if(params.containsKey('imageEn') && params["imageEn"] == "delete")
        {
            banner.setImageEn("")
        }else{
            if(params.containsKey('imageEn') && params["imageEn"] != "")
            {
                banner.setImageEn(params["imageEn"])
            }else{
                banner.setImageEn(oldFileNameEn)
            }
        }
        if(params.containsKey('imageCh') && params["imageCh"] == "delete")
        {
            banner.setImageCh("")
        }else{
            if(params.containsKey('imageCh') && params["imageCh"] != "")
            {
                banner.setImageCh(params["imageCh"])
            }else{
                banner.setImageCh(oldFileNameCh)
            }
        }
        if (!banner.save())
        {
            flash.error = "Have Error."
            render(view: "editForm",id:params.id, model: [banner: banner])
        }else{
            if(params.containsKey('imageTh') && params["imageTh"] != "")
            {
                uploadService.deleteFile(oldFileNameTh,'banner')
                if(params.containsKey('imageTh') && params["imageTh"] != "delete")
                {
                    uploadService.moveFile(banner.getImageTh(),'temp','banner')
                }
            }
            if(params.containsKey('imageEn') && params["imageEn"] != "")
            {
                uploadService.deleteFile(oldFileNameEn,'banner')
                if(params.containsKey('imageEn') && params["imageEn"] != "delete")
                {
                    uploadService.moveFile(banner.getImageEn(),'temp','banner')
                }
            }
            if(params.containsKey('imageCh') && params["imageCh"] != "")
            {
                uploadService.deleteFile(oldFileNameCh,'banner')
                if(params.containsKey('imageCh') && params["imageCh"] != "delete")
                {
                    uploadService.moveFile(banner.getImageCh(),'temp','banner')
                }
            }
            flash.message = "Update Complete."
            redirect(action: "index")
        }
    }

    def delete() {
        def result = [status:true]
        Banner banner = Banner.get(params.id)
        if (!banner)
        {
            result["message"] = "Have Error."
            result["status"] = false
        }else{
            String fileDelTh = banner.getImageTh()
            String fileDelEn = banner.getImageEn()
            String fileDelCh = banner.getImageCh()
            banner.delete()
            result["message"] = "Delete Complete."
            if(fileDelTh != "")
            {
                uploadService.deleteFile(fileDelTh,'banner')
            }
            if(fileDelEn != "")
            {
                uploadService.deleteFile(fileDelEn,'banner')
            }
            if(fileDelCh != "")
            {
                uploadService.deleteFile(fileDelCh,'banner')
            }
        }
        render result as JSON
    }

    def list() {
        String sortlabel = ""
        Integer sortvalue = 1
        String searchtxt = ""
        List listColumns = ["titleTh","titleEn","titleCh","sequence","active"]
        if(!params.containsKey('iSortCol_0'))
        {
            params.iSortCol_0 = 0
            params.sSortDir_0 = "asc"
            params.sSearch = ""
            params.sEcho = 1
            params.iDisplayStart = 0
            params.iDisplayLength = 10
        }
        if(params["iSortCol_0"].toInteger() <= (listColumns.size() - 1))
        {
            sortlabel = listColumns.getAt(params["iSortCol_0"].toInteger())
        }
        def dataList = Banner.findAllByTitleThIlikeOrTitleEnIlikeOrTitleChIlike("%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%",[max: params["iDisplayLength"].toInteger(),sort: sortlabel,order: params["sSortDir_0"],offset: params["iDisplayStart"].toInteger()])
        def totalRecord = Banner.countByTitleThIlikeOrTitleEnIlikeOrTitleChIlike("%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%")
        def sourcedatatable = [sEcho:params["sEcho"].toInteger()]
        sourcedatatable["iTotalRecords"] = totalRecord
        sourcedatatable["iTotalDisplayRecords"] = totalRecord
        List nData = []
        dataList.each {
            List iData = [it.getTitleTh(),it.getTitleEn(),it.getTitleCh(),it.getSequence(),it.getActive(),"<a href='${createLink(action: "editForm",id: it.getId())}' class='btn btn-default'>Edit</a> <a href='javascript:void(0);' onclick=\"alertDel(${it.getId()},'${it.getTitleTh()}')\" class='btn btn-danger'>Delete</a>"]
            nData.push(iData)
        }
        sourcedatatable["aaData"] = nData
        render sourcedatatable as JSON
    }

    def saveSort(){
        def idList = params.sortList.split(",")
        def result = [status:true]
        def sortNumber = 1
        idList.each{
            Banner banner = Banner.get(it)
            banner.setSequence(sortNumber)
            banner.save()
            sortNumber++
        }
        render result as JSON
    }
}
