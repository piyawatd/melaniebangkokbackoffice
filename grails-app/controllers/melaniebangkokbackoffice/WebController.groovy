package melaniebangkokbackoffice

import grails.converters.JSON

class WebController {

    def index() {
        def langSelect = checkLang(params)
        def newsList = News.list(max: 3)
        def galleryList = Gallery.list(max: 6)
        def bannerList = Banner.list()
        [lang:langSelect,newsList: newsList, galleryLists: galleryList, bannerList: bannerList]
    }

    def news() {
        def langSelect = checkLang(params)
        def newsLists = News.list()
        [lang:langSelect,newsLists: newsLists]
    }

    def newsDetail() {
        def langSelect = checkLang(params)
        News news = News.get(params.id)
        def newsLists = News.list(max: 5)
        [lang:langSelect,news: news, newsLists: newsLists]
    }

    def executive() {
        def langSelect = checkLang(params)
        def executive = Page.findByAlias("executive")
        [lang:langSelect,executive:executive ]
    }

    def gallery() {
        def langSelect = checkLang(params)
        def galleryLists = Gallery.list()
        [lang:langSelect,galleryLists: galleryLists]
    }

    def galleryList() {
        def langSelect = checkLang(params)
        Gallery gallery = Gallery.get(params.id)
        def galleryImage = GalleryImage.findAllByGallery(gallery, [sort: "sequence", order: 'asc'])
        [lang:langSelect,galleryImage: galleryImage, gallery: gallery]
    }

    def contact() {
        def langSelect = checkLang(params)
        def contact = Page.findByAlias("contact")
        [lang:langSelect,contact:contact]
    }

    def thankyou() {
        
    }

    def recordContact() {
        Contact contact = new Contact(params)
        contact.properties = params
        if (!contact.save()) {
            flash.error = "Have Error."
            redirect(action: "contact")
        }
        else {
            flash.message = "Successful"
            redirect(action: "index")
        }
    }

    def project() {
        def langSelect = checkLang(params)
        def concept = Page.findByAlias("concept")
        def exclusivity = Page.findByAlias("exclusivity")
        def design = Page.findByAlias("design")
        def transportation = Page.findByAlias("transportation")
        def facility = Page.findByAlias("facility")
        def location = Page.findByAlias("location")
        [lang:langSelect,concept:concept,exclusivity:exclusivity,design:design,transportation:transportation,facility:facility,location:location]
    }

    def plan() {
        def langSelect = checkLang(params)
        [lang:langSelect]

    }

    def developer() {

        def langSelect = checkLang(params)
        Page page = Page.findByAlias("develop")
        [lang:langSelect,page:page]

    }

    def register() {


    }

    def recordRegister() {
//        println(params)
        Register register = new Register(params)
        register.properties = params
        if (!register.save()) {
            flash.error = "Have Error."
            redirect(action: "register")
        }
        else {
            flash.message = "Successful"
            def priceRange = ""
            switch (params.get("priceRange")) {
                case "1":
                    priceRange = "Below 4 Mil.Baht"
                    break;
                case "2":
                    priceRange = "4-5 Mil.Baht"
                    break;
                case "3":
                    priceRange = "5.1-6 Mil.Baht"
                    break;
                case "4":
                    priceRange = "6.1-7 Mil.Baht"
                    break;
                case "5":
                    priceRange = "7.1-8 Mil.Baht"
                    break;
                case "6":
                    priceRange = "8.1-9 Mil.Baht"
                    break;
                case "7":
                    priceRange = "9.1-10 Mil.Baht"
                    break;
                case "8":
                    priceRange = "10 Mil. Baht"
                    break;
            }

            def unitType = ""
            switch (params.get("unitType")) {
                case "1":
                    unitType = "1 Bed"
                    break;
                case "2":
                    unitType = "2 Bed"
                    break;
            }

            def unitSize = ""
            switch (params.get("unitSize")) {
                case "1":
                    unitSize = "Below 35";
                    break;
                case "2":
                    unitSize = "35-40";
                    break;
                case "3":
                    unitSize = "41-50";
                    break;
                case "4":
                    unitSize = "51-60";
                    break;
                case "5":
                    unitSize = "61-70";
                    break;
                case "6":
                    unitSize = "71-80";
                    break;
                case "7":
                    unitSize = "81-90";
                    break;
                case "8":
                    unitSize = "91-100";
                    break;
                case "9":
                    unitSize = "Above 100";
                    break;
            }

            def goal = ""
            switch (params.get("goal")) {
                case "1":
                    goal = "จุดประสงค์ในการซื้อ"
                    break;
                case "2":
                    goal = "บ้านหลังแรก"
                    break;
                case "3":
                    goal = "บ้านหลังที่สอง"
                    break;
                case "4":
                    goal = "เพื่อลงทุน (ขายต่อ)"
                    break;
                case "5":
                    goal = "เพื่อลงทุน (ปล่อยเช่า)"
                    break;
                case "6":
                    goal = "เพื่ออยู่อาศัยเอง"
                    break;
                case "7":
                    goal = "ใกล้ที่ทำงาน"
                    break;
            }


            def showCase = ""
            if (params.get("showCase") == "on")
            {
                showCase = "รับชมตัวอย่างห้องโครงการ"
            }
            else {
                showCase = "ไม่รับชมตัวอย่างห้องโครงการ"
            }
            sendMail {
                to "${params["email"]}","Pong@primegatesrealty.com","Chantira@primegatesrealty.com","Suppaluk@primegatesrealty.com","sjedt@3ddaily.com","worajedt@icloud.com"
//                to "${params["email"]}"
                subject "Thank you for your registration - MelanieBangkok"
                from "webmaster@primegatesrealty.com"
                body(view: "../emailTemplate/confirmRegister",
                        model: [firstname: "${params["firstName"]}", lastname: "${params["lastName"]}", mobile: "${params["tel"]}",
                                email    : "${params["email"]}", price: "${priceRange}",
                                type     : "${unitType}", size: "${unitSize}", goal: "${goal}", showCase: "${showCase}"])
                redirect(action: "thankyou")
            }
        }
    }

	def checkLang(def params)
	{
        def langSelect = "Th"
        if (params.containsKey('lang')) {
            session.setAttribute("lang", params.lang)
            if(params.lang == "zh_Cn")
            {
                langSelect = "Ch"
            }else{
                langSelect = params.lang
            }
        } else {
            if (!session.getAttribute("lang")) {
                session.setAttribute("lang", "Th")
            }else{
	            if(session.lang == "zh_Cn")
	            {
    	            langSelect = "Ch"
	            }else{
		            langSelect = session.lang		            
	            }

            }
        }
        return langSelect		
	}
}
