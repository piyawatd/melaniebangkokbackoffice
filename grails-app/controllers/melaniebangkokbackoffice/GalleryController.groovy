package melaniebangkokbackoffice

import grails.converters.JSON

class GalleryController {

    def uploadService

    def index() {

    }

    def show() {
        def gallery = Gallery.get(params.id)
        withFormat {
            html {}
            json {
                if(params.containsKey('callback'))
                {
                    render "${params.callback}(${gallery as JSON})"
                }else{
                    render gallery as JSON
                }
            }
        }
    }

    def newForm() {
        Gallery gallery = new Gallery()
        def galleryImages = []
        [gallery:gallery,galleryImages:galleryImages]
    }

    def newImage() {
        Gallery gallery = Gallery.get(params.id)
        GalleryImage galleryImage = new GalleryImage()
        [gallery:gallery,galleryImage:galleryImage]
    }

    def editForm() {
        Gallery gallery = Gallery.get(params.id)
        def galleryImages = GalleryImage.findAllByGallery(gallery,[sort: "sequence",order:"asc"])
        [gallery:gallery,galleryImages:galleryImages]
    }

    def editImage() {
        Gallery gallery = Gallery.get(params.id)
        GalleryImage galleryImage = GalleryImage.get(params.iId)
        [gallery:gallery,galleryImage:galleryImage]
    }

    def create() {
        Gallery gallery = new Gallery(params)
        def galleryImage = []
        if (!gallery.save())
        {
            render(view: "newForm", model: [gallery: gallery,galleryImage: galleryImage])
        }else{
            if(params.containsKey('imageName') && params["imageName"] != "")
            {
                uploadService.moveFile(gallery.getImageName(),'temp','gallery')
            }
            flash.message = "Create Complete."
            redirect(action: "editForm",id: gallery.getId())
        }
    }

    def update() {
        Gallery gallery = Gallery.get(params.id)
        String oldFileName = gallery.getImageName()
        def galleryImage = gallery.getGalleryImages()
        gallery.properties = params
        if(!params.containsKey('imageName') || params["imageName"] == "")
        {
            gallery.setImageName(oldFileName)
        }
        if (!gallery.save())
        {
            flash.error = "Have Error."
            render(view: "editForm",id:params.id, model: [gallery: gallery,galleryImage: galleryImage])
        }else{
            if(params.containsKey('imageName') && params["imageName"] != "")
            {
                uploadService.deleteFile(oldFileName,'gallery')
                if(params.containsKey('imageName') && params["imageName"] != "delete")
                {
                    uploadService.moveFile(gallery.getImageName(),'temp','gallery')
                }
            }
            flash.message = "Update Complete."
            redirect(action: "editForm",id: gallery.getId())
        }
    }

    def createImage() {
        Gallery gallery = Gallery.get(params.id)
        GalleryImage galleryImage = new GalleryImage(params)
        galleryImage.setGallery(gallery)
        galleryImage.setSequence(GalleryImage.countByGallery(gallery).toInteger()+1)
        if (!galleryImage.save())
        {
            gallery.errors.allErrors.each {
                println it
            }
            flash.error = "Have Error"
            render(view: "newImage",id: params.id, model: [gallery: gallery,galleryImage:galleryImage])
        }else{
            if(params.containsKey('imageName') && params["imageName"] != "")
            {
                uploadService.moveFile(galleryImage.getImageName(),'temp','gallery')
            }
            flash.message = "Create Image Complete."
            redirect(action: "editForm",id: params.id)
        }
    }

    def updateImage() {
        if(params.containsKey('imageName') && params["imageName"] != "")
        {
            Gallery gallery = Gallery.get(params.id)
            GalleryImage galleryImage = GalleryImage.get(params.iId)
            String oldFileNameTh = galleryImage.getImageName()
            galleryImage.properties = params
            if (!galleryImage.save())
            {
                flash.error = "Have Error."
                render(view: "editImage",id:params.id,params:[iId:params.iId], model: [gallery: gallery,galleryImage: galleryImage])
            }else{
                uploadService.deleteFile(oldFileNameTh,'gallery')
                if(params.containsKey('imageName'))
                {
                    uploadService.moveFile(galleryImage.getImageName(),'temp','gallery')
                }
                flash.message = "Update Complete."
            }
        }
        redirect(action: "editForm",id:params.id)
    }

    def delete() {
        def result = [status:true]
        Gallery gallery = Gallery.get(params.id)
        if(gallery) {
            if (gallery.getGalleryImages().size() > 0) {
                result["message"] = "Have image in gallery."
                result["status"] = false
            } else {
                String fileDel = gallery.getImageName()
                result["message"] = "Delete Complete."
                if(fileDel != "")
                {
                    uploadService.deleteFile(fileDel,'gallery')
                }
                gallery.delete()
            }
        }else{
            result["message"] = "Wrong Id."
            result["status"] = false
        }
        render result as JSON
    }

    def deleteImage() {
        def result = [status:true]
        GalleryImage galleryImage = GalleryImage.get(params.id)
        if(galleryImage)
        {
            String fileDel = galleryImage.getImageName()
            galleryImage.delete()
            if(fileDel != "")
            {
                uploadService.deleteFile(fileDel,'gallery')
            }
        }else{
            result["message"] = "Wrong Image Id."
            result["status"] = false
        }
        render result as JSON
    }

    def list() {
        String sortlabel = "titleTh"
        Integer sortvalue = 1
        String searchtxt = ""
        List listColumns = ["titleTh","titleEn","titleCh"]
        if(!params.containsKey('iSortCol_0'))
        {
            params.iSortCol_0 = 0
            params.sSortDir_0 = "asc"
            params.sSearch = ""
            params.sEcho = 1
            params.iDisplayStart =0
            params.iDisplayLength = 10
        }
        if(params["iSortCol_0"].toInteger() <= (listColumns.size() - 2))
        {
            sortlabel = listColumns.getAt(params["iSortCol_0"].toInteger())
        }
        def dataList = Gallery.findAllByTitleThIlikeOrTitleEnIlikeOrTitleChIlike("%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%",[max: params["iDisplayLength"].toInteger(),sort: sortlabel,order: params["sSortDir_0"],offset: params["iDisplayStart"].toInteger()])
        def totalRecord = Gallery.countByTitleThIlikeOrTitleEnIlikeOrTitleChIlike("%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%")
        def sourcedatatable = [sEcho:params["sEcho"].toInteger()]
        sourcedatatable["iTotalRecords"] = totalRecord
        sourcedatatable["iTotalDisplayRecords"] = totalRecord
        List nData = []
        dataList.each {
            List iData = [it.getTitleTh(),it.getTitleEn(),it.getTitleCh(),"<a href='${createLink(action: "editForm",id: it.getId())}' class='btn btn-default'>Edit</a> <a href='javascript:void(0);' onclick=\"alertDel(${it.getId()},'${it.getTitleTh()}')\" class='btn btn-danger'>Delete</a>"]
            nData.push(iData)
        }
        sourcedatatable["aaData"] = nData
        render sourcedatatable as JSON
    }

    def saveSort(){
        def idList = params.sortList.split(",")
        def result = [status:true]
        def sortNumber = 1
        idList.each{
            GalleryImage galleryImage = GalleryImage.get(it)
            galleryImage.setSequence(sortNumber)
            galleryImage.save()
            sortNumber++
        }
        render result as JSON
    }
}
