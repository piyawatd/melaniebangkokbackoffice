package melaniebangkokbackoffice

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: "web",action: "index")
        "/manual"(controller: "manualweb",action: "index")
        "/contact"(controller: "web",action: "contact")
        "/developer"(controller: "web",action: "developer")
        "/executive"(controller: "web",action: "executive")
        "/gallery"(controller: "web",action: "gallery")
        "/galleryList/$id"(controller: "web",action: "galleryList")
        "/news"(controller: "web",action: "news")
        "/newsDetail/$id"(controller: "web",action: "newsDetail")
        "/plan"(controller: "web",action: "plan")
        "/project"(controller: "web",action: "project")
        "/register"(controller: "web",action: "register")
        "/recordRegister"(controller: "web",action: "recordRegister",method:"POST")
        "/admin"(controller: "users",action: "login")
        "/admin/login"(controller: "users",action: "login")
        "/admin/logout"(controller: "users",action: "logout")

        group "/admin/users", {
            "/"(controller: "users",action: "index")
            "/new"(controller: "users",action: "newForm")
            "/edit/$id"(controller: "users",action: "editForm")
        }

        group "/admin/news", {
            "/"(controller: "news",action: "index",method: "GET")
            "/new"(controller: "news",action: "newForm",method: "GET")
            "/edit/$id"(controller: "news",action: "editForm",method: "GET")
            "/delete/$id"(controller:"news",action:"delete",method:"POST")
        }

        group "/admin/page", {
            "/"(controller: "page",action: "index")
            "/new"(controller: "page",action: "newForm")
            "/edit/$id"(controller: "page",action: "editForm")
            "/delete/$id"(controller:"page",action:"delete",method:"POST")
        }

        group "/admin/contact", {
            "/"(controller: "contact",action: "index")
            "/delete/$id"(controller:"contact",action:"delete",method:"POST")
        }

        group "/admin/banner", {
            "/"(controller: "banner",action: "index")
            "/new"(controller: "banner",action: "newForm")
            "/edit/$id"(controller: "banner",action: "editForm")
            "/delete/$id"(controller:"banner",action:"delete",method:"POST")
        }

        group "/admin/register", {
            "/"(controller: "register",action: "index")
            "/delete/$id"(controller:"register",action:"delete",method:"POST")
        }

        group "/admin/gallery", {
            "/"(controller:"gallery",action:"index",method:"GET")
            "/list"(controller:"gallery",action:"list",method:"GET")
            "/newForm"(controller:"gallery",action:"newForm",method:"GET")
            "/editForm/$id"(controller:"gallery",action:"editForm",method:"GET")
            "/newImage/$id"(controller:"gallery",action:"newImage",method:"GET")
            "/editImage/$id/$iId"(controller:"gallery",action:"editImage",method:"GET")
            "/create"(controller:"gallery",action:"create",method:"POST")
            "/update/$id"(controller:"gallery",action:"update",method:"POST")
            "/delete/$id"(controller:"gallery",action:"delete",method:"POST")
            "/createImage/$id"(controller:"gallery",action:"createImage",method:"POST")
            "/updateImage/$id/$iId"(controller:"gallery",action:"updateImage",method:"POST")
            "/deleteImage/$id"(controller:"gallery",action:"deleteImage",method:"POST")
            "/saveSort"(controller:"gallery",action:"saveSort",method:"POST")
        }

//        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
