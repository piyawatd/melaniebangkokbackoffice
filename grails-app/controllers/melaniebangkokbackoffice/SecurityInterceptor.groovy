package melaniebangkokbackoffice


class SecurityInterceptor {

    SecurityInterceptor() {
        matchAll()
                .except(controller:'users', action:'login')
                .except(controller:'web')
                .except(controller:'web',action:'recordRegister')
                .except(controller:'temail',action: 'index')
                .except(controller:'manualweb')
                .except(uri:'/uploadresource/**')
                .except(uri:'/uploads/**')
                .except(uri:'/sitemap.xml')
    }

    boolean before() {
        if (!session.users && actionName != "login") {
            redirect(controller: "users", action: "login")
            return false
        }
        return true
    }
//
//    boolean after() { true }
//
//    void afterView() {
//        // no-op
//    }
}
