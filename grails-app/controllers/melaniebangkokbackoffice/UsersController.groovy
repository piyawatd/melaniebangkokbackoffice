package melaniebangkokbackoffice

import grails.converters.JSON
import groovy.sql.Sql

class UsersController {

    def dataSource

    def login() {
        if (request.get) {
            return // render the login view
        }

        String passEncode = params.password.encodeAsSHA1()
        Users u = Users.findByUsername(params.username)
        if (u) {
            if (u.getPassword() == passEncode) {
                session.users = u
                flash.message = "Login success."
                redirect(controller: "register", action: "index")
            }
            else {
                flash.error = "Password incorrect"
                render(view: "login", model: [message: "Password incorrect"])
            }
        }
        else {
            flash.error = "User not found"
            render(view: "login", model: [message: "User not found"])
        }
    }

    def logout(){
        session.removeAttribute("users")
        flash.message = "Logout success."
        redirect(action: "login")

    }

    def index(){

    }

    def newForm(){
        Users users = new Users()
        [users:users]
    }

    def editForm(){
        Users users = Users.get(params.id)
        [users:users]
    }

    def create(){
        if(params.containsKey("password") && params.password != "") {
            Users users = new Users(params)
            if (!users.save()) {
                flash.error = "Have Error."
                render(views: "newForm", model: [users: users])
            } else {
                flash.message = "Create complete."
            }
            redirect(action: "index")
        }else{
            flash.error = "Password not blank."
            render(views:"newForm",model: [users:users])
        }
    }

    def update(){
        if(params.containsKey("password") && params.password != "")
        {
            Users users = Users.get(params.id)
            users.properties = params
            if(!users.save())
            {
                flash.error = "Have Error."
                render(views:"editForm",id:params.id,model: [users:users])
            }else{
                flash.message = "Update complete."
            }
        }else{
            String query = $/
                update users set
                firstname = '${params.firstname}',
                lastname = '${params.lastname}',
                email = '${params.email}'
                where id = ${params.id}
            /$
            Sql sql = new Sql(dataSource)
            sql.execute(query)
            flash.message = "Update complete."
        }
        redirect(action: "index")
    }

    def delete(){
        def result = [status:true]
        Users users = Users.get(params.id)
        if (!users)
        {
            result["message"] = "Have Error."
            result["status"] = false
        }else{
            users.delete()
            result["message"] = "Delete Complete."
        }
        render result as JSON
    }

    def list(){
        String sortlabel = "username"
        Integer sortvalue = 1
        String searchtxt = ""
        List listColumns = ["username","firstName","lastName","email","userLevel"]
        if(!params.containsKey('iSortCol_0'))
        {
            params.iSortCol_0 = 0
            params.sSortDir_0 = "asc"
            params.sSearch = ""
            params.sEcho = 1
            params.iDisplayStart =0
            params.iDisplayLength = 10
        }
        if(params["iSortCol_0"].toInteger() <= (listColumns.size() - 2))
        {
            sortlabel = listColumns.getAt(params["iSortCol_0"].toInteger())
        }
        def newsList = Users.findAllByUsernameIlikeOrFirstNameIlikeOrLastNameIlikeOrEmailIlike("%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%",[max: params["iDisplayLength"].toInteger(),sort: sortlabel,order: params["sSortDir_0"],offset: params["iDisplayStart"].toInteger()])
        def newsTotal = Users.countByUsernameIlikeOrFirstNameIlikeOrLastNameIlikeOrEmailIlike("%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%")
        def sourcedatatable = [sEcho:params["sEcho"].toInteger()]
        sourcedatatable["iTotalRecords"] = newsTotal
        sourcedatatable["iTotalDisplayRecords"] = newsTotal
        List nData = []
        newsList.each {
            String uLevel = ""
            switch (it.getUserLevel())
            {
                case 0:
                    uLevel = "User"
                    break
                case 1:
                    uLevel = "Admin"
                    break
                case 2:
                    uLevel = "Saadmin"
                    break
            }
            List iData = [it.getUsername(),it.getFirstName(),it.getLastName(),it.getEmail(),uLevel,"<a href='${createLink(action: "editForm",id: it.getId())}' class='btn btn-default'>Edit</a> <a href='javascript:void(0);' onclick=\"alertDel(${it.getId()},'${it.getUsername()}')\" class='btn btn-danger'>Delete</a>"]
            nData.push(iData)
        }
        sourcedatatable["aaData"] = nData
        render sourcedatatable as JSON
    }
}
