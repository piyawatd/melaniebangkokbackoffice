package melaniebangkokbackoffice

import grails.converters.JSON

class NewsController {

    def uploadService

    def index() {

    }

    def show() {
        String mypass = "mypass"
        println mypass.encodeAsSHA1()
//        def news = News.last()
//        withFormat {
//            html {}
//            json {
//                if(params.containsKey('callback'))
//                {
//                    render "${params.callback}(${news as JSON})"
//                }else{
//                    render news as JSON
//                }
//            }
//        }
    }

    def newForm() {
        News news = new News()
        [news:news]
    }

    def editForm() {
        News news = News.get(params.id)
        [news:news]
    }

    def create() {
        News news = new News(params)
        if (!news.save())
        {
            flash.error = "Have Error"
            render(view: "newForm", model: [news: news])
        }else{
            if(params.containsKey('imageTh') && params["imageTh"] != "")
            {
                uploadService.moveFile(news.getImageTh(),'temp','news')
            }
            if(params.containsKey('imageEn') && params["imageEn"] != "")
            {
                uploadService.moveFile(news.getImageEn(),'temp','news')
            }
            if(params.containsKey('imageCh') && params["imageCh"] != "")
            {
                uploadService.moveFile(news.getImageCh(),'temp','news')
            }
            flash.message = "Create Complete."
            redirect(action: "index")
        }
    }

    def update() {
        News news = News.get(params.id)
        String oldFileNameTh = news.getImageTh()
        String oldFileNameEn = news.getImageEn()
        String oldFileNameCh = news.getImageCh()
        news.properties = params
        if(params.containsKey('imageTh') && params["imageTh"] == "delete")
        {
            news.setImageTh("")
        }else{
            if(params["imageTh"] == "")
            {
                news.setImageTh(oldFileNameTh)
            }
        }
        if(params.containsKey('imageEn') && params["imageEn"] == "delete")
        {
            news.setImageEn()
        }else{
            if(params["imageEn"] == "")
            {
                news.setImageEn(oldFileNameEn)
            }
        }
        if(params.containsKey('imageCh') && params["imageCh"] == "delete")
        {
            news.setImageCh()
        }else{
            if(params["imageCh"] == "")
            {
                news.setImageCh(oldFileNameCh)
            }
        }
        if (!news.save())
        {
            flash.error = "Have Error."
            render(view: "editForm",id:params.id, model: [news: news])
        }else{
            if(params.containsKey('imageTh') && params["imageTh"] != "")
            {
                uploadService.deleteFile(oldFileNameTh,'news')
                if(params.containsKey('imageTh') && params["imageTh"] != "delete")
                {
                    uploadService.moveFile(news.getImageTh(),'temp','news')
                }
            }
            if(params.containsKey('imageEn') && params["imageEn"] != "")
            {
                uploadService.deleteFile(oldFileNameEn,'news')
                if(params.containsKey('imageEn') && params["imageEn"] != "delete")
                {
                    uploadService.moveFile(news.getImageEn(),'temp','news')
                }
            }
            if(params.containsKey('imageCh') && params["imageCh"] != "")
            {
                uploadService.deleteFile(oldFileNameCh,'news')
                if(params.containsKey('imageCh') && params["imageCh"] != "delete")
                {
                    uploadService.moveFile(news.getImageCh(),'temp','news')
                }
            }
            flash.message = "Update Complete."
            redirect(action: "editForm",id:params.id)
        }
    }

    def delete() {
        def result = [status:true]
        News news = News.get(params.id)
        if (!news)
        {
            result["message"] = "Have Error."
            result["status"] = false
        }else{
            String fileDelTh = news.getImageTh()
            String fileDelEn = news.getImageEn()
            String fileDelCh = news.getImageCh()
            news.delete()
            result["message"] = "Delete Complete."
            if(fileDelTh != "")
            {
                uploadService.deleteFile(fileDelTh,'news')
            }
            if(fileDelEn != "")
            {
                uploadService.deleteFile(fileDelEn,'news')
            }
            if(fileDelCh != "")
            {
                uploadService.deleteFile(fileDelCh,'news')
            }
        }
        render result as JSON
    }

    def list() {
        String sortlabel = "titleTh"
        Integer sortvalue = 1
        String searchtxt = ""
        List listColumns = ["titleTh","titleEn","titleCh"]
        if(!params.containsKey('iSortCol_0'))
        {
            params.iSortCol_0 = 0
            params.sSortDir_0 = "asc"
            params.sSearch = ""
            params.sEcho = 1
            params.iDisplayStart =0
            params.iDisplayLength = 10
        }
        if(params["iSortCol_0"].toInteger() <= (listColumns.size() - 2))
        {
            sortlabel = listColumns.getAt(params["iSortCol_0"].toInteger())
        }
        def newsList = News.findAllByTitleThIlikeOrTitleEnIlikeOrTitleChIlike("%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%",[max: params["iDisplayLength"].toInteger(),sort: sortlabel,order: params["sSortDir_0"],offset: params["iDisplayStart"].toInteger()])
        def newsTotal = News.countByTitleThIlikeOrTitleEnIlikeOrTitleChIlike("%${params["sSearch"]}%","%${params["sSearch"]}%","%${params["sSearch"]}%")
        def sourcedatatable = [sEcho:params["sEcho"].toInteger()]
        sourcedatatable["iTotalRecords"] = newsTotal
        sourcedatatable["iTotalDisplayRecords"] = newsTotal
        List nData = []
        newsList.each {
            List iData = [it.getTitleTh(),it.getTitleEn(),it.getTitleCh(),"<a href='${createLink(action: "editForm",id: it.getId())}' class='btn btn-default'>Edit</a> <a href='javascript:void(0);' onclick=\"alertDel(${it.getId()},'${it.getTitleTh()}')\" class='btn btn-danger'>Delete</a>"]
            nData.push(iData)
        }
        sourcedatatable["aaData"] = nData
        render sourcedatatable as JSON
    }
}
