package melaniebangkokbackoffice

import grails.converters.JSON
import grails.util.Holders

class UploadController {

    def uploadService

    def index() { }

    def image() {
        def result = [status:true]
        def folderPath = "temp"
        def fileName = uploadService.uploadFile(request,"Filedata[]",folderPath,"${params.module}_",params.fileType)
        if(fileName == "")
        {
            result["status"] = false
            result.put("filename","wrong file type.")
        }else{
            result.put("filename",fileName)
            result.put("pathfile",resource(dir: "uploadresource/${folderPath}/", file: fileName))
        }
        render result as JSON
    }
}
